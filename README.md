# Parachute checkers / Parasutisticka dama

### Implementace deskové hry Parasutisticka dama v jazyce JAVA.

Implementace hry Parasutisticka dama, je rocnikovym projektem ve druhem rocniku studia Informatiky na Univerzite Palackeho v Olomouci. 
Hlavnim cilem hry, je zajmout (preskocit) souperovy figurky. Hra je implementovana v jazyce Java s vyuzitim vyvojoveho prostredi NetBeans.
Pro svuj beh tedy potrebuje nainstalovanou Javu, ve verzi alespon 1.7.0. Pro grafické uživatelské rozhraní je pouzita knihovna Swing.