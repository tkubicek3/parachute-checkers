/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package history;

import java.awt.Point;
import java.util.ArrayList;
import parachutecheckers.CheckersBoard;
import parachutecheckers.Control;
import parachutecheckers.Rules;

/**
 *
 * @author KubiX
 */
public class OperationsWithCheckersBoard {

    private static Point from;
    private static Point to;
    private static boolean isFirstPointInit;
    private static boolean isSecondPointInit;
    private static int playerOnMove;

    public static String compareTwoBoardsForGetMove(CheckersBoard olderBoard, CheckersBoard newerBoard) {

        int olderWhiteFigureCount = olderBoard.getBox(0, 6);
        int olderBlackFigureCount = olderBoard.getBox(1, 1);
        //
        int newerWhiteFigureCount = newerBoard.getBox(0, 6);
        int newerBlackFigureCount = newerBoard.getBox(1, 1);
        //
        boolean lockWhite = false;
        boolean lockBlack = false;
        //
        isFirstPointInit = false;
        isSecondPointInit = false;
        //
        playerOnMove = 0;
        //
        from = new Point(-1, -1);
        to = new Point(-1, -1);

        if (newerWhiteFigureCount != olderWhiteFigureCount) {
            lockWhite = true;
        }
        if (newerBlackFigureCount != olderBlackFigureCount) {
            lockBlack = true;
        }
        int num = 0;
        for (int x = 0;
                x < olderBoard.getBoardSize();
                x++) {
            for (int y = 0; y < olderBoard.getBoardSize(); y++) {
                if (num % 2 != 0) {
                    int olderBox = olderBoard.getBox(x, y);
                    int newerBox = newerBoard.getBox(x, y);

                    if (newerBox != olderBox) {
                        if (olderBox > 0 && !lockWhite) {
                            setFromToPoints(x, y, Rules.WHITE_PLAYER);
                        }
                        if (olderBox < 0 && !lockBlack) {
                            setFromToPoints(x, y, Rules.BLACK_PLAYER);
                        }
                        if (newerBox > 0 && !lockWhite) {
                            setFromToPoints(x, y, Rules.WHITE_PLAYER);
                        }
                        if (newerBox < 0 && !lockBlack) {
                            setFromToPoints(x, y, Rules.BLACK_PLAYER);
                        }
                    }
                }

                if (y == 7) {
                    num++;
                }
                num++;
            }
        }

        int fromBox = olderBoard.getBox(from);
        int toBox = olderBoard.getBox(to);

        getDirectionAndSwapPoints(fromBox, toBox, playerOnMove);

        String moveOrJump = "Tah ";
        if (lockWhite || lockBlack) {
            moveOrJump = "Skok ";
        }
        moveOrJump += getFigureString(fromBox, toBox, playerOnMove);

        String move = moveOrJump + " z " + convertNumberToLetter(from.y) + "" + (8 - from.x) + " na " + convertNumberToLetter(to.y) + "" + (8 - to.x);

        return move;
    }

    private static void setFromToPoints(int x, int y, int player) {
        if (!isFirstPointInit) {
            playerOnMove = player;
            from = new Point(x, y);
            isFirstPointInit = true;
        } else if (!isSecondPointInit) {
            to = new Point(x, y);
            isSecondPointInit = true;
        }
    }

    private static void getDirectionAndSwapPoints(int fromBox, int toBox, int playerOnMove) {
        if (fromBox == CheckersBoard.WHITE_PARACHUTIST || fromBox == CheckersBoard.WHITE_CHECKERS) {
            swapFromToPoints();
        }
        if (toBox == CheckersBoard.BLACK_PARACHUTIST || toBox == CheckersBoard.BLACK_CHECKERS) {
            swapFromToPoints();
        }
        if (playerOnMove == Rules.WHITE_PLAYER) {
            swapFromToPoints();
        }
    }

    private static void swapFromToPoints() {
        Point tmp = new Point(from);
        from = new Point(to);
        to = new Point(tmp);
    }

    private static String getFigureString(int fromBox, int toBox, int playerOnMove) {

        String figureString = "";

        if (playerOnMove == Rules.WHITE_PLAYER) {
            if (fromBox == CheckersBoard.WHITE_PAWN || toBox == CheckersBoard.WHITE_PAWN) {
                figureString += "bílým pěšcem";
            } else if (fromBox == CheckersBoard.WHITE_PARACHUTIST || toBox == CheckersBoard.WHITE_PARACHUTIST) {
                figureString += "bílým parašut.";
            } else if (fromBox == CheckersBoard.WHITE_CHECKERS || toBox == CheckersBoard.WHITE_CHECKERS) {
                figureString += "bílou dámou";
            }
        } else if (playerOnMove == Rules.BLACK_PLAYER) {
            if (fromBox == CheckersBoard.BLACK_PAWN || toBox == CheckersBoard.BLACK_PAWN) {
                figureString += "černým pěšcem";
            } else if (fromBox == CheckersBoard.BLACK_CHECKERS || toBox == CheckersBoard.BLACK_CHECKERS) {
                figureString += "černou dámou";
            } else if (fromBox == CheckersBoard.BLACK_PARACHUTIST || toBox == CheckersBoard.BLACK_PARACHUTIST) {
                figureString += "černým parašut.";
            }
        }

        return figureString;
    }

    private static String convertNumberToLetter(int number) {
        switch (number) {
            case (0):
                return "A";
            case (1):
                return "B";
            case (2):
                return "C";
            case (3):
                return "D";
            case (4):
                return "E";
            case (5):
                return "F";
            case (6):
                return "G";
            case (7):
                return "H";
            default:
                return null;
        }
    }
}