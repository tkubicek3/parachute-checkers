package history;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import parachutecheckers.*;

public class History {

    private List<CheckersBoard> listOfHistory;
    private List<CheckersBoard> listOfRedoMoves;

    public History() {
        this.listOfHistory = new ArrayList<>();
        this.listOfRedoMoves = new ArrayList<>();
    }

    public void addToHistory(CheckersBoard board) {
        listOfHistory.add(setCheckersBoardToSave(board.clone(), Control.againFromPosition));
    }

    public boolean isPossibleUndoMove() {
        return !listOfHistory.isEmpty();
    }

    public boolean isPossibleRedoMove() {
        return !listOfRedoMoves.isEmpty();
    }

    public CheckersBoard getUndoMove(CheckersBoard board) {
        if (isPossibleUndoMove()) {
            CheckersBoard historyBoard = listOfHistory.get(listOfHistory.size() - 1);
            listOfHistory.remove(listOfHistory.size() - 1);

            addToRedoMoves(board);
            CheckersBoard undoBoard = setCheckersBoardToLoad(historyBoard.clone());

            return undoBoard;
        }

        return null;
    }

    public void addToRedoMoves(CheckersBoard board) {
        listOfRedoMoves.add(setCheckersBoardToSave(board.clone(), Control.againFromPosition));
    }

    public CheckersBoard getRedoMove(CheckersBoard board) {
        if (isPossibleRedoMove()) {
            CheckersBoard historyBoard = listOfRedoMoves.get(listOfRedoMoves.size() - 1);
            listOfRedoMoves.remove(listOfRedoMoves.size() - 1);

            addToHistory(board);

            CheckersBoard redoBoard = setCheckersBoardToLoad(historyBoard.clone());

            return redoBoard;
        }

        return null;
    }

    public void clearRedoMoves() {
        listOfRedoMoves.clear();
    }

    public CheckersBoard setCheckersBoardToSave(CheckersBoard board, Point againFrom) {

        CheckersBoard savedBoard = board.clone();

        savedBoard.setBox(0, 0, againFrom.x);
        savedBoard.setBox(0, 2, againFrom.y);
        savedBoard.setBox(0, 4, Rules.getPlayerOnMove());
        savedBoard.setBox(0, 6, board.getWhiteFigureCount());
        savedBoard.setBox(1, 1, board.getBlackFigureCount());
        savedBoard.setBox(1, 3, board.getDodgeCount());

        return savedBoard;
    }

    public static CheckersBoard setCheckersBoardToLoad(CheckersBoard board) {

        Control.againFromPosition = new Point(board.getBox(0, 0), board.getBox(0, 2));
        board.setBox(0, 0, 0);
        board.setBox(0, 2, 0);
        Rules.setPlayerOnMove(board.getBox(0, 4));
        board.setBox(0, 4, 0);

        board.setWhiteFigureCount(board.getBox(0, 6));
        board.setBlackFigureCount(board.getBox(1, 1));
        board.setBox(0, 6, 0);
        board.setBox(1, 1, 0);

        board.setDodgeCount(board.getBox(1, 3));
        board.setBox(1, 3, 0);

        return board;
    }

    public void reInitialize() {
        this.listOfHistory = new ArrayList<>();
        this.listOfRedoMoves = new ArrayList<>();
    }

    public List<CheckersBoard> getListOfHistory() {
        return listOfHistory;
    }

    public List<CheckersBoard> getListOfRedoMoves() {
        return listOfRedoMoves;
    }

    public void setListOfHistory(List<CheckersBoard> listOfHistory) {
        this.listOfHistory = listOfHistory;
    }

    public void setListOfRedoMoves(List<CheckersBoard> listOfRedoMoves) {
        this.listOfRedoMoves = listOfRedoMoves;
    }
}
