package history;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import mediator.Game;
import parachutecheckers.*;

public class SaveAndLoad {

    public static void saveGame(Writer writer, CheckersBoard board) throws Exception {
        CheckersBoard savedBoard = setCheckersBoardToSave(board, Control.againFromPosition);
        saveBoards(writer, savedBoard);
    }

    public static CheckersBoard loadGame(Reader r) throws Exception {
        return setCheckersBoardToLoad(loadBoards(r));
    }

    private static void saveBoards(Writer writer, CheckersBoard board) throws Exception {

        int crc = 0;

        List<CheckersBoard> listOfHistory = new ArrayList<>(Game.getHistory().getListOfHistory());
        List<CheckersBoard> listOfRedoMoves = new ArrayList<>(Game.getHistory().getListOfRedoMoves());

        try (BufferedWriter buffWriter = new BufferedWriter(writer)) {

            buffWriter.write("ActualGame");
            crc += getCRCfromString("ActualGame");
            buffWriter.newLine();
            buffWriter.newLine();

            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    int number = board.getBox(i, j);
                    crc += getCRCfromInt(number) + number * 3;
                    buffWriter.write(number + " ");
                }
                buffWriter.newLine();
            }

            buffWriter.newLine();
            buffWriter.write("HistoryMoves");
            crc += getCRCfromString("HistoryMoves");
            buffWriter.newLine();
            buffWriter.newLine();

            for (CheckersBoard checkersBoard : listOfHistory) {
                for (int i = 0; i < 8; i++) {
                    for (int j = 0; j < 8; j++) {
                        int number = checkersBoard.getBox(i, j);
                        crc += getCRCfromInt(number) + number * 4;
                        buffWriter.write(number + " ");
                    }
                    buffWriter.newLine();
                }
                buffWriter.newLine();
            }

            buffWriter.newLine();
            buffWriter.write("RedoMoves");
            crc += getCRCfromString("RedoMoves");
            buffWriter.newLine();
            buffWriter.newLine();

            for (CheckersBoard checkersBoard : listOfRedoMoves) {
                for (int i = 0; i < 8; i++) {
                    for (int j = 0; j < 8; j++) {
                        int number = checkersBoard.getBox(i, j);
                        crc += getCRCfromInt(number) + number * 5;
                        buffWriter.write(number + " ");
                    }
                    buffWriter.newLine();
                }
                buffWriter.newLine();
            }

            buffWriter.newLine();
            buffWriter.write("CRC");
            crc += getCRCfromString("CRC");
            buffWriter.newLine();
            buffWriter.newLine();

            buffWriter.write(Integer.toHexString(crc));

        } catch (Exception e) {
        }
    }

    private static CheckersBoard loadBoards(Reader r) throws Exception {

        String[] lineOfNumbers;
        int crc = 0;
        String crcFromFile = "";

        Queue<Integer> actualGame = new LinkedList<>();
        Queue<Integer> historyMoves = new LinkedList<>();
        Queue<Integer> redoMoves = new LinkedList<>();

        boolean stringActualGame = false;
        boolean stringHistoryMoves = false;
        boolean stringRedo = false;
        boolean stringCRC = false;

        try (BufferedReader buffReader = new BufferedReader(r)) {

            while (buffReader.ready()) {

                lineOfNumbers = buffReader.readLine().split("[\\ ]+");

                for (String string : lineOfNumbers) {
                    switch (string) {
                        case (""):
                            continue;
                        case ("ActualGame"):
                            crc += getCRCfromString("ActualGame");
                            stringActualGame = true;
                            continue;
                        case ("HistoryMoves"):
                            crc += getCRCfromString("HistoryMoves");
                            stringActualGame = false;
                            stringHistoryMoves = true;
                            continue;
                        case ("RedoMoves"):
                            crc += getCRCfromString("RedoMoves");
                            stringActualGame = false;
                            stringHistoryMoves = false;
                            stringRedo = true;
                            continue;
                        case ("CRC"):
                            crc += getCRCfromString("CRC");
                            stringActualGame = false;
                            stringHistoryMoves = false;
                            stringRedo = false;
                            stringCRC = true;
                            continue;
                    }

                    if (stringActualGame) {
                        actualGame.add(Integer.parseInt(string));
                    } else if (stringHistoryMoves) {
                        historyMoves.add(Integer.parseInt(string));
                    } else if (stringRedo) {
                        redoMoves.add(Integer.parseInt(string));
                    } else if (stringCRC) {
                        crcFromFile = (string);
                    }
                }
            }

        } catch (Exception e) {
        }

        //ActualBoard//
        CheckersBoard actualBoard = new CheckersBoard();

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                int figure = actualGame.poll();
                crc += getCRCfromInt(figure) + figure * 3;
                actualBoard.setBox(i, j, figure);
            }
        }
        //---//

        //History//
        List<CheckersBoard> listOfHistory = new ArrayList<>();

        int velUndo = historyMoves.size() / 64;

        for (int l = velUndo; l > 0; l--) {
            CheckersBoard board = new CheckersBoard();
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    int figure = historyMoves.poll();
                    crc += getCRCfromInt(figure) + figure * 4;
                    board.setBox(i, j, figure);
                }
            }
            listOfHistory.add(board);
        }
        //---//

        //RedoMoves//
        List<CheckersBoard> listOfRedoMoves = new ArrayList<>();

        int velRedo = redoMoves.size() / 64;

        for (int l = velRedo; l > 0; l--) {
            CheckersBoard board = new CheckersBoard();
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    int figure = redoMoves.poll();
                    crc += getCRCfromInt(figure) + figure * 5;
                    board.setBox(i, j, figure);
                }
            }
            listOfRedoMoves.add(board);
        }
        //---//

        if (!Integer.toHexString(crc).equals(crcFromFile)) {
            throw new LoadException();
        }

        Game.getHistory().setListOfHistory(listOfHistory);
        Game.getHistory().setListOfRedoMoves(listOfRedoMoves);

        List<CheckersBoard> listOfHistoryForGetMoves = new ArrayList<>(listOfHistory);
        listOfHistoryForGetMoves.add(actualBoard);

        List<CheckersBoard> listOfRedosForGetMoves = new ArrayList<>(listOfRedoMoves);
        listOfRedosForGetMoves.add(listOfHistoryForGetMoves.get(listOfHistoryForGetMoves.size() - 1));

        Game.getgUserInterface().getInfoPanel().reInitializeTextArea();

        for (int i = 0; i < listOfHistoryForGetMoves.size() - 1; i++) {
            CheckersBoard olderBoard = listOfHistoryForGetMoves.get(i);
            CheckersBoard newerBoard = listOfHistoryForGetMoves.get(i + 1);

            Game.getgUserInterface().getInfoPanel().addMoveToTextArea(
                    OperationsWithCheckersBoard.compareTwoBoardsForGetMove(olderBoard, newerBoard));
        }

        int infoPanelMove = Game.getgUserInterface().getInfoPanel().getMove();
        int infoPanelSelect = Game.getgUserInterface().getInfoPanel().getTextAreaMovesPrinter().getSelectedIndex();

        for (int i = listOfRedosForGetMoves.size() - 1; i > 0; i--) {
            CheckersBoard olderBoard = listOfRedosForGetMoves.get(i);
            CheckersBoard newerBoard = listOfRedosForGetMoves.get(i - 1);

            Game.getgUserInterface().getInfoPanel().addMoveToTextArea(
                    OperationsWithCheckersBoard.compareTwoBoardsForGetMove(olderBoard, newerBoard));
        }

        Game.getgUserInterface().getInfoPanel().setMove(infoPanelMove);
        Game.getgUserInterface().getInfoPanel().getTextAreaMovesPrinter().select(infoPanelSelect);

        return actualBoard;
    }

    private static int getCRCfromString(String string) {
        int crc = 0;

        for (int i = 0; i < string.length(); i++) {
            crc += string.charAt(i);
        }

        return crc;
    }

    private static int getCRCfromInt(int figure) {

        switch (figure) {
            case (CheckersBoard.WHITE_PAWN):
                return 11;
            case (CheckersBoard.WHITE_PARACHUTIST):
                return 37;
            case (CheckersBoard.WHITE_CHECKERS):
                return 53;
            case (CheckersBoard.BLACK_PAWN):
                return 43;
            case (CheckersBoard.BLACK_PARACHUTIST):
                return 29;
            case (CheckersBoard.BLACK_CHECKERS):
                return 19;
            case (1000):
                return 61;
            default:
                return 3;
        }
    }

    public static CheckersBoard setCheckersBoardToSave(CheckersBoard board, Point againFrom) {

        CheckersBoard savedBoard = board.clone();

        savedBoard.setBox(0, 0, againFrom.x);
        savedBoard.setBox(0, 2, againFrom.y);
        savedBoard.setBox(0, 4, Rules.getPlayerOnMove());
        savedBoard.setBox(0, 6, board.getWhiteFigureCount());
        savedBoard.setBox(1, 1, board.getBlackFigureCount());
        savedBoard.setBox(1, 3, board.getDodgeCount());
        savedBoard.setBox(1, 5, Control.WHITE_PLAYER_SETTING);
        savedBoard.setBox(1, 7, Control.BLACK_PLAYER_SETTING);

        return savedBoard;
    }

    public static CheckersBoard setCheckersBoardToLoad(CheckersBoard board) {

        Control.againFromPosition = new Point(board.getBox(0, 0), board.getBox(0, 2));
        board.setBox(0, 0, 0);
        board.setBox(0, 2, 0);
        Rules.setPlayerOnMove(board.getBox(0, 4));
        board.setBox(0, 4, 0);

        board.setWhiteFigureCount(board.getBox(0, 6));
        board.setBlackFigureCount(board.getBox(1, 1));
        board.setBox(0, 6, 0);
        board.setBox(1, 1, 0);

        board.setDodgeCount(board.getBox(1, 3));
        board.setBox(1, 3, 0);

        Control.WHITE_PLAYER_SETTING = board.getBox(1, 5);
        Control.BLACK_PLAYER_SETTING = board.getBox(1, 7);

        board.setBox(1, 5, 0);
        board.setBox(1, 7, 0);

        return board;
    }
}
