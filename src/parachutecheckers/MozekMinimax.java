package parachutecheckers;

import java.awt.Point;
import java.util.Stack;
import mediator.Game;

public class MozekMinimax {

    public Stack<CheckersBoard> UNDO = new Stack<>();

    public  void pridejDoHistorie(CheckersBoard d) {

        CheckersBoard b =    Game.getHistory().setCheckersBoardToSave(d, Control.againFromPosition);

        UNDO.add(b);
    }

    public CheckersBoard undo() {

        if (UNDO.isEmpty()) {
            System.out.println("\nZAVAZNA CHYBA.");
            System.exit(0);
        }
        
        CheckersBoard b = vratDesku(UNDO.pop());

        return b;
    }

    public CheckersBoard vratDesku(CheckersBoard d) {

        Control.againFromPosition = new Point(d.getBox(0, 0), d.getBox(0, 2));
        d.setBox(0, 0, 0);
        d.setBox(0, 2, 0);
        Rules.setPlayerOnMove(d.getBox(0, 4));
        d.setBox(0, 4, 0);

        d.setWhiteFigureCount(d.getBox(0, 6));
        d.setBlackFigureCount(d.getBox(1, 1));
        d.setBox(0, 6, 0);
        d.setBox(1, 1, 0);

        d.setDodgeCount(d.getBox(1, 3));
        d.setBox(1, 3, 0);
        
        return d;
    }
}