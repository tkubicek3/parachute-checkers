package parachutecheckers;

import java.awt.Point;
import java.io.*;
import java.util.List;

public class UI {

    public static Point from;
    public static Point where;
    public static final String END = "END";
    public static final String SETPLAYERS = "NASTAVHRACE";
    public static final String HELP = "HELP";
    public static final String UNDO = "UNDO";
    public static final String REDO = "REDO";
    public static final String SAVE = "SAVE";
    public static final String LOAD = "LOAD";

    public void newGame() throws IOException {

        BufferedReader keyReader = new BufferedReader(new InputStreamReader(System.in));
        String command;

        CheckersBoard checkersBoard = new CheckersBoard();
        checkersBoard.initializeBoard();

        loadPlayersSettings();

        while (true) {

            System.out.print("\nI-I-I-I-I-I-I-I-I-I-I-I-I-I-I-I-I-I-I-I");
            checkersBoard.printBoard();



            if (Rules.getPlayerOnMove() == Rules.WHITE_PLAYER) {
                System.out.println("Na rade je bily hrac.");
            } else if (Rules.getPlayerOnMove() == Rules.BLACK_PLAYER) {
                System.out.println("Na rade je cerny hrac.");
            }

            System.out.print("Co chcete udelat(enter pro tah, HELP pro napovedu): ");
            command = keyReader.readLine().toUpperCase();

            if (command.length() != 0) {
                switch (command) {
                    case (END):
                        System.out.println("Hra byla ukoncena.");
                        System.exit(0);
                        break;
                    case (SETPLAYERS):
                        loadPlayersSettings();
                        continue;
                    case (UNDO):
                        continue;
                    case (LOAD):
                        try (FileReader r = new FileReader("saveGame.txt")) {
                         //   checkersBoard = SaveAndLoad.loadGame(r);
                            System.out.println("Hra byla nactena.");
                        } catch (Exception e) {
                            System.err.println(e.getMessage());
                        }
                        continue;
                    case (HELP):
                        System.out.println("");
                        System.out.println("Mozne prikazy: \n"
                                + END + " - pro konec hry\n"
                                + SETPLAYERS + " - pro nastaveni hrace jako hrajiciho cloveka nebo pocitac\n"
                                + UNDO + " - krok zpet\n"
                                + REDO + " - krok vpred\n"
                                + SAVE + " - ulozi rozehranou hru\n"
                                + LOAD + " - nacte rozehranou hru\n");
                        continue;
                    default:
                        continue;
                }
            }

            if (Control.isPlayerComputer(Rules.getPlayerOnMove())) {  // PC hrac
                List<Point> tah = Mozek.inteligentniVyber(Rules.getPlayerOnMove(), Mozek.HLOUBKA, checkersBoard, 1);

                System.out.print("TAH: ");
                Control.printJump(tah);

                Control.gameControl(tah.get(0), tah.get(1), checkersBoard);
                continue;                                          //-------------//
            }

            from = readMove("Odkud: ");
            where = readMove("Kam: ");

            System.out.println("");

            Control.gameControl(from, where, checkersBoard);
        }
    }

    public static Point readMove(String s) throws IOException {

        String readBox;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        //
        int line;
        int column;

        while (true) {

            System.out.print(s);

            readBox = bufferedReader.readLine().toUpperCase();

            if (readBox.length() != 2) {
                System.out.print("Chybny vstup, ");
                continue;
            }

            try {
                column = Integer.parseInt(readBox.substring(1)) - 1;
                line = readBox.charAt(0);
            } catch (Exception e) {
                System.out.print("Chybny vstup, ");
                continue;
            }

            if (((line < 65) || (line > 72)) || (readBox.length() < 2)) {
                System.out.print("Chybny vstup, ");
                continue;
            }

            if (((column < 0) || (column > 7)) || (readBox.length() < 2)) {
                System.out.print("Chybny vstup, ");
                continue;
            }
            Point a = new Point(new Integer(readBox.charAt(1) - 49), new Integer(readBox.toUpperCase().charAt(0) - 65));
            System.out.println(a.getX()+", "+a.getY());
            return a;
        }
    }

    public static void loadPlayersSettings() throws IOException {

        String readPlayer;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        int nactenyBily;
        int nactenyCerny;

        System.out.println("\nBily hrac: ");
        System.out.println("1. Clovek");
        System.out.println("2. Pocitac");
        System.out.print("Vase volba: ");

        while (true) {

            readPlayer = bufferedReader.readLine();

            try {
                nactenyBily = Integer.parseInt(readPlayer);
            } catch (Exception e) {
                System.out.print("Chybny vstup, znovu: ");
                continue;
            }

            if (nactenyBily == Control.HUMAN_PLAYER || nactenyBily == Control.COMPUTER_PLAYER) {
                break;
            } else {
                System.out.print("Chybny vstup, znovu: ");
            }
        }

        System.out.println("");
        System.out.println("Cerny hrac: ");
        System.out.println("1. Clovek");
        System.out.println("2. Pocitac");
        System.out.print("Vase volba: ");

        while (true) {

            readPlayer = bufferedReader.readLine();

            try {
                nactenyCerny = Integer.parseInt(readPlayer);
            } catch (Exception e) {
                System.out.print("Chybny vstup, znovu: ");
                continue;
            }

            if (nactenyCerny == Control.HUMAN_PLAYER || nactenyCerny == Control.COMPUTER_PLAYER) {
                break;
            } else {
                System.out.print("Chybny vstup, znovu: ");
            }
            System.out.println(Control.HUMAN_PLAYER);
            System.out.println(Control.COMPUTER_PLAYER);
        }

        Control.setPlayers(nactenyBily, nactenyCerny);
    }
}
