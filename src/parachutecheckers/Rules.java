package parachutecheckers;

import java.awt.Point;
import java.util.*;

public class Rules {

    public static final int WHITE_PLAYER = 1;
    public static final int BLACK_PLAYER = -1;
    public static final int MAX_COUNT_OF_DODGE = 30;
    //
    private static int playerOnMove = 0;
    //

    public static int getPlayerOnMove() {
        return playerOnMove;
    }

    public static void setPlayerOnMove(int player) {
        playerOnMove = player;
    }

    public static void setNextPlayer() {
        playerOnMove = playerOnMove * (-1);
    }

    public static boolean isPlayerOnMove(int player) {
        if (player == playerOnMove) {
            return true;
        }
        return false;
    }

    public static List<Point> generatorTahu(Point odkud, CheckersBoard d, int hrac, boolean debug) {

        List<Point> mozneTahy = new ArrayList();

        switch (d.getBox(odkud)) {
            case (CheckersBoard.WHITE_PAWN):
            case (CheckersBoard.BLACK_PAWN):
                mozneTahy = pawnOrParachutistMoveGenerator(odkud, d, hrac, false, false);
                break;

            case (CheckersBoard.WHITE_PARACHUTIST):
            case (CheckersBoard.BLACK_PARACHUTIST):
                mozneTahy = pawnOrParachutistMoveGenerator(odkud, d, hrac, true, true);
                break;

            case (CheckersBoard.WHITE_CHECKERS):
            case (CheckersBoard.BLACK_CHECKERS):
                mozneTahy = generatorTahuDamy(odkud, d, hrac, false);
                break;
        }

        if (debug) {
            printPossiblePosition(mozneTahy, odkud);
        }

        return mozneTahy;
    }

    public static List<Point> pawnOrParachutistMoveGenerator(Point from, CheckersBoard b, int player, boolean onlyJumps, boolean parachutist) {

        List<Point> possibleMoves = new ArrayList<>();
        List<Point> movesWithoutJumps = new ArrayList<>();
        List<Point> movesWithJumps = new ArrayList<>();

        Point kamJedna;
        Point kamDva;
        Point kamJedna2;
        Point kamDva2;

        if (parachutist) {
            kamJedna = shiftPosition(from, 1, 1, player);
            kamDva = shiftPosition(from, 1, -1, player);
        } else {
            kamJedna = shiftPosition(from, -1, 1, player);
            kamDva = shiftPosition(from, -1, -1, player);
        }

        //PRVNI
        if (!onlyJumps) {
            if ((isIndexInArray(kamJedna)) && (b.getPlayerFromFigure(kamJedna) == CheckersBoard.EMPTY_BOX)) {
                movesWithoutJumps.add(kamJedna);
            }
        }

        if ((isIndexInArray(kamJedna)) && (b.getPlayerFromFigure(kamJedna) == player * (-1))) {
            if (parachutist) {
                kamJedna2 = shiftPosition(from, 2, 2, player);
            } else {
                kamJedna2 = shiftPosition(from, -2, 2, player);
            }

            if ((isIndexInArray(kamJedna2)) && (b.getPlayerFromFigure(kamJedna2) == CheckersBoard.EMPTY_BOX)) {
                movesWithJumps.add(kamJedna2);
                onlyJumps = true;
            }
        }

        //DRUHY
        if (!onlyJumps) {
            if ((isIndexInArray(kamDva)) && (b.getPlayerFromFigure(kamDva) == CheckersBoard.EMPTY_BOX)) {
                movesWithoutJumps.add(kamDva);
            }
        }

        if ((isIndexInArray(kamDva)) && (b.getPlayerFromFigure(kamDva) == player * (-1))) {
            if (parachutist) {
                kamDva2 = shiftPosition(from, 2, -2, player);
            } else {
                kamDva2 = shiftPosition(from, -2, -2, player);
            }

            if ((isIndexInArray(kamDva2)) && (b.getPlayerFromFigure(kamDva2) == CheckersBoard.EMPTY_BOX)) {
                movesWithJumps.add(kamDva2);
            }
        }

        if (!movesWithJumps.isEmpty()) {
            return movesWithJumps;
        } else {
            return movesWithoutJumps;
        }
    }

    public static Point shiftPosition(Point where, int plusX, int plusY, int hrac) {
        return new Point(where.x + (plusX * hrac), where.y + plusY);
    }

    public static List<Point> generatorTahuDamy(Point odkud, CheckersBoard d, int hrac, boolean jenSkoky) {

        List<Point> mozneTahy = new ArrayList<>();

        List<Point> vLevoNahoru = diagonalToTheLeftAndUp(odkud);
        List<Point> vPravoNahoru = diagonalToTheRightAndUp(odkud);
        List<Point> vPravoDolu = diagonalToTheRightAndBottom(odkud);
        List<Point> vLevoDolu = diagonalToTheLeftAndBottom(odkud);

        List<Point> tahyBezSkoku = new ArrayList<>();
        List<Point> tahySeSkokem = new ArrayList<>();

        for (int i = 0; i < vLevoNahoru.size(); i++) {
            if (!jenSkoky) {
                if (d.getPlayerFromFigure(vLevoNahoru.get(i)) == 0) {
                    tahyBezSkoku.add(vLevoNahoru.get(i));
                    continue;
                }
            }
            if (d.getPlayerFromFigure(vLevoNahoru.get(i)) == hrac) {
                break;
            }

            if (d.getPlayerFromFigure(vLevoNahoru.get(i)) == hrac * (-1)) {
                Point za = new Point(vLevoNahoru.get(i).x - 1, vLevoNahoru.get(i).y - 1);
                if ((isIndexInArray(za)) && (d.getPlayerFromFigure(za) == 0)) {
                    tahySeSkokem.addAll(pridejTahy(vLevoNahoru.get(i), vLevoNahoru.get(vLevoNahoru.size() - 1), d));
                    jenSkoky = true;
                }
                break;
            }
        }

        for (int i = 0; i < vPravoNahoru.size(); i++) {
            if (!jenSkoky) {
                if (d.getPlayerFromFigure(vPravoNahoru.get(i)) == 0) {
                    tahyBezSkoku.add(vPravoNahoru.get(i));
                    continue;
                }
            }
            if (d.getPlayerFromFigure(vPravoNahoru.get(i)) == hrac) {
                break;
            }

            if (d.getPlayerFromFigure(vPravoNahoru.get(i)) == hrac * (-1)) {
                Point za = new Point(vPravoNahoru.get(i).x - 1, vPravoNahoru.get(i).y + 1);
                if ((isIndexInArray(za)) && (d.getPlayerFromFigure(za) == 0)) {
                    tahySeSkokem.addAll(pridejTahy(vPravoNahoru.get(i), vPravoNahoru.get(vPravoNahoru.size() - 1), d));
                    jenSkoky = true;
                }
                break;
            }
        }

        for (int i = 0; i < vPravoDolu.size(); i++) {
            if (!jenSkoky) {
                if (d.getPlayerFromFigure(vPravoDolu.get(i)) == 0) {
                    tahyBezSkoku.add(vPravoDolu.get(i));
                    continue;
                }
            }
            if (d.getPlayerFromFigure(vPravoDolu.get(i)) == hrac) {
                break;
            }

            if (d.getPlayerFromFigure(vPravoDolu.get(i)) == hrac * (-1)) {
                Point za = new Point(vPravoDolu.get(i).x + 1, vPravoDolu.get(i).y + 1);
                if ((isIndexInArray(za)) && (d.getPlayerFromFigure(za) == 0)) {
                    tahySeSkokem.addAll(pridejTahy(vPravoDolu.get(i), vPravoDolu.get(vPravoDolu.size() - 1), d));
                    jenSkoky = true;
                }
                break;
            }
        }

        for (int i = 0; i < vLevoDolu.size(); i++) {
            if (!jenSkoky) {
                if (d.getPlayerFromFigure(vLevoDolu.get(i)) == 0) {
                    tahyBezSkoku.add(vLevoDolu.get(i));
                    continue;
                }
            }
            if (d.getPlayerFromFigure(vLevoDolu.get(i)) == hrac) {
                break;
            }

            if (d.getPlayerFromFigure(vLevoDolu.get(i)) == hrac * (-1)) {
                Point za = new Point(vLevoDolu.get(i).x + 1, vLevoDolu.get(i).y - 1);
                if ((isIndexInArray(za)) && (d.getPlayerFromFigure(za) == 0)) {
                    tahySeSkokem.addAll(pridejTahy(vLevoDolu.get(i), vLevoDolu.get(vLevoDolu.size() - 1), d));
                }
                break;
            }
        }

        if (!tahySeSkokem.isEmpty()) {
            return tahySeSkokem;
        }

        return tahyBezSkoku;
    }

    public static List<Point> pridejTahy(Point odkud, Point kam, CheckersBoard d) {
        List<Point> mozneTahy = new ArrayList<>();
        int x = odkud.x;
        int y = odkud.y;

        if ((odkud.getX() - kam.getX() > 0) && (odkud.getY() - kam.getY() > 0)) {
            while ((kam.getX() != x) && (d.getPlayerFromFigure(new Point(x - 1, y - 1)) == 0)) {
                mozneTahy.add(new Point(x - 1, y - 1));
                x--;
                y--;
            }
        }
        if ((odkud.getX() - kam.getX() > 0) && (odkud.getY() - kam.getY() < 0)) {
            while ((kam.getX() != x) && (d.getPlayerFromFigure(new Point(x - 1, y + 1)) == 0)) {
                mozneTahy.add(new Point(x - 1, y + 1));
                x--;
                y++;
            }
        }
        if ((odkud.getX() - kam.getX() < 0) && (odkud.getY() - kam.getY() < 0)) {
            while ((kam.getX() != x) && (d.getPlayerFromFigure(new Point(x + 1, y + 1)) == 0)) {
                mozneTahy.add(new Point(x + 1, y + 1));
                x++;
                y++;
            }
        }
        if ((odkud.getX() - kam.getX() < 0) && (odkud.getY() - kam.getY() > 0)) {
            while ((kam.getX() != x) && (d.getPlayerFromFigure(new Point(x + 1, y - 1)) == 0)) {
                mozneTahy.add(new Point(x + 1, y - 1));
                x++;
                y--;
            }
        }

        return mozneTahy;
    }

    public static boolean vicenasobnySkok(Point odkud, CheckersBoard d, int hrac) {

        List<Point> mozneTahy = new ArrayList();

        switch (d.getBox(odkud)) {
            case (CheckersBoard.WHITE_PAWN):
            case (CheckersBoard.BLACK_PAWN):
                mozneTahy = pawnOrParachutistMoveGenerator(odkud, d, hrac, true, false);
                break;

            case (CheckersBoard.WHITE_PARACHUTIST):
            case (CheckersBoard.BLACK_PARACHUTIST):
                mozneTahy = pawnOrParachutistMoveGenerator(odkud, d, hrac, true, true);
                break;

            case (CheckersBoard.WHITE_CHECKERS):
            case (CheckersBoard.BLACK_CHECKERS):
                mozneTahy = generatorTahuDamy(odkud, d, hrac, true);
                break;
        }

        if (mozneTahy.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    public static List<Point> kontrolaMoznehoSkokuCiVyhry(boolean skok, int hrac, CheckersBoard d) {

        List<Point> mozneSkokyPescu = new ArrayList();
        List<Point> mozneSkokyDam = new ArrayList();

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (d.getPlayerFromFigure(new Point(i, j)) == hrac) {
                    Point p = new Point(i, j);

                    switch (d.getBox(p)) {
                        case (CheckersBoard.WHITE_PAWN):
                        case (CheckersBoard.BLACK_PAWN):
                            if (!(pawnOrParachutistMoveGenerator(p, d, hrac, skok, false).isEmpty())) {
                                mozneSkokyPescu.add(p);
                            }
                            break;

                        case (CheckersBoard.WHITE_PARACHUTIST):
                        case (CheckersBoard.BLACK_PARACHUTIST):
                            if (!(pawnOrParachutistMoveGenerator(p, d, hrac, true, true).isEmpty())) {
                                mozneSkokyPescu.add(p);
                            }
                            break;

                        case (CheckersBoard.WHITE_CHECKERS):
                        case (CheckersBoard.BLACK_CHECKERS):
                            if (!(generatorTahuDamy(p, d, hrac, skok).isEmpty())) {
                                mozneSkokyDam.add(p);
                            }
                            break;
                    }
                }
            }
        }

        if (!(mozneSkokyDam.isEmpty()) && skok) {
            return mozneSkokyDam;
        } else {
            mozneSkokyPescu.addAll(mozneSkokyDam);
        }

        return mozneSkokyPescu;
    }

    public static CheckersBoard changeToCheckerOrPawn(Point where, int player, CheckersBoard board) {
        if (board.isPawn(where)) {
            if (where.getX() == 0 || where.getX() == 7) {
                board.setBox(where, CheckersBoard.CHECKERS * (player));
            }
        }
        if (board.isParachutist(where)) {
            board.setBox(where, CheckersBoard.PAWN * (player));
        }
        return board;
    }

    ////////////////////////////////////////////////////////////////////////////
    public static boolean isIndexInArray(Point where) {
        if ((where.getX() >= 0 && where.getX() <= 7) && (where.getY() >= 0 && where.getY() <= 7)) {
            return true;
        }
        return false;
    }

    public static void printPossiblePosition(List<Point> listOfPosition, Point from) {
        System.out.print("Mozne tahy z pozice (" + ((char) (from.getY() + 65)) + "" + (from.getX() + 1) + ") jsou na: ");
        for (Point p : listOfPosition) {
            System.out.print("(" + ((char) (p.getY() + 65)) + "" + (p.getX() + 1) + ")");
        }
        System.out.println("");
        
        System.out.print("Mozne tahy z pozice (" +  (from.getY()) + "" + (from.getX()) + ") jsou na: ");
        for (Point p : listOfPosition) {
            System.out.print("(" + (p.getY()) + "" + (p.getX()) + ")");
        }
        System.out.println("");
    }

    public static List<Point> diagonalToTheRightAndUp(Point where) {

        List<Point> positionsList = new ArrayList();
        int x = where.x - 1;
        int y = where.y + 1;
        while (isIndexInArray(new Point(x, y))) {
            positionsList.add(new Point(x, y));
            x--;
            y++;
        }
        return positionsList;
    }

    public static List<Point> diagonalToTheLeftAndUp(Point where) {

        List<Point> positionsList = new ArrayList();
        int x = where.x - 1;
        int y = where.y - 1;
        while (isIndexInArray(new Point(x, y))) {
            positionsList.add(new Point(x, y));
            x--;
            y--;
        }
        return positionsList;
    }

    public static List<Point> diagonalToTheRightAndBottom(Point where) {

        List<Point> positionsList = new ArrayList();
        int x = where.x + 1;
        int y = where.y + 1;
        while (isIndexInArray(new Point(x, y))) {
            positionsList.add(new Point(x, y));
            x++;
            y++;
        }
        return positionsList;
    }

    public static List<Point> diagonalToTheLeftAndBottom(Point where) {

        List<Point> positionsList = new ArrayList();
        int x = where.x + 1;
        int y = where.y - 1;
        while (isIndexInArray(new Point(x, y))) {
            positionsList.add(new Point(x, y));
            x++;
            y--;
        }
        return positionsList;
    }
}
