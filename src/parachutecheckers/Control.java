package parachutecheckers;

import java.awt.Point;
import java.util.List;
import mediator.Game;

public class Control {

    public static final int HUMAN_PLAYER = 1;
    public static final int COMPUTER_PLAYER = 2;
    //
    public static int WHITE_PLAYER_SETTING = 0;
    public static int BLACK_PLAYER_SETTING = 0;
    //
    public static Point againFromPosition = new Point(1000, 1000); //pozice,
    //ze ktere je nutne tahnout znovu, pro vicenasobnem skoku apod.
    //
    //
    //***//Vypis tahu
    public static boolean addMove = false;
    //***//

    //Dodelat boolean againFromPosition
    public static CheckersBoard gameControl(Point from, Point where, CheckersBoard b) {

        int player = b.getPlayerFromFigure(from);
        int countOfPlayersFigures = b.getFigureCount(player, b);

        List<Point> listOfPossibleJump = Rules.kontrolaMoznehoSkokuCiVyhry(true, player, b);

        if (b.getBox(where) == CheckersBoard.EMPTY_BOX) {
            Control.addMove = false;
            if (!Control.isPlayerComputer(Rules.getPlayerOnMove())) {
                if (!(Rules.isPlayerOnMove(player))) {     //Jiny hrac na tahu  !
                    if (player == CheckersBoard.EMPTY_BOX) {
                        System.out.println("Na policku neni zadna figurka.");
                    } else {
                        System.out.println("Tah nelze provest, protoze na rade jiny hrac.");
                    }
                } else if (Rules.isIndexInArray(againFromPosition) && !(againFromPosition.equals(from))) {
                    System.out.print("Tah neni mozny, je nutno znovu skocit z ");
                    System.out.print("(" + ((char) (againFromPosition.getY() + 65)) + "" + (againFromPosition.getX() + 1) + ")");
                } else if ((!(listOfPossibleJump.isEmpty())) && (!(listOfPossibleJump.contains(from)))) { //Je nutno skakat
                    System.out.print("Tah neni mozny, je nutno skocit z ");
                    printJump(listOfPossibleJump);
                } else if (!(Rules.generatorTahu(from, b, player, true).contains(where))) {  //Tah neni v seznamu moznych tahu.
                    System.out.println("Vas tah neni mozny.");
                } else {
                    Game.getHistory().addToHistory(b);
                    Game.getHistory().clearRedoMoves();
                    //***//
                    addMove = true;
                    //***//

                    moveFigure(from, where, b); //presune figurku
                    deleteFigure(from, where, player, b);  //smaze preskocene
                    if ((countOfPlayersFigures == b.getFigureCount(player, b)) || (Rules.vicenasobnySkok(where, b, player))) { //vicensabny skok - kontrola
                        Rules.changeToCheckerOrPawn(where, player, b); // zmena na damu ci na pesce
                        Rules.setNextPlayer();
                        Control.againFromPosition = new Point(1000, 1000);
                    } else {
                        //Je mozny dalsi skok, hraj znovu !
                        Control.againFromPosition = where; //pro kontrolu spravne vybrane pozice odkud pri V skoku
                    }
                }

            } else { ///jen pro pc - hrace
                Game.getHistory().addToHistory(b);
                Game.getHistory().clearRedoMoves();
                //
                Rules.generatorTahu(from, b, player, false); //kvuli dalsim skokum
                moveFigure(from, where, b); //presune figurku
                //***///
                addMove = true;
                //***//
                deleteFigure(from, where, player, b);  //smaze preskocene
                if ((countOfPlayersFigures == b.getFigureCount(player, b)) || (Rules.vicenasobnySkok(where, b, player))) { //vicensabny skok - kontrola
                    Rules.changeToCheckerOrPawn(where, player, b); // zmena na damu ci na pesce
                    Rules.setNextPlayer();
                    Control.againFromPosition = new Point(1000, 1000);
                } else {
                    //Je mozny dalsi skok, hraj znovu !
                    Control.againFromPosition = where; //pro kontrolu spravne vybrane pozice odkud pri V skoku
                }
            }
        }

        return b;
    }

    public static CheckersBoard moveFigure(Point from, Point where, CheckersBoard b) {

        int valueOfBoxFrom = b.getBox(from.x, from.y);

        b.setBox(where, valueOfBoxFrom);

        return b;
    }

    public static CheckersBoard deleteFigure(Point from, Point where, int player, CheckersBoard b) {

        boolean isDodge = true;

        b.setBox(from, CheckersBoard.EMPTY_BOX);

        int fromX = from.x;
        int fromY = from.y;
        int whereX = where.x;
        int whereY = where.y;

        if ((fromX - whereX > 0) && (fromY - whereY > 0)) {
            while ((whereX != fromX)) {
                if (b.getPlayerFromFigure(new Point(fromX, fromY)) == player * (-1)) {
                    b.setBox(fromX, fromY, CheckersBoard.EMPTY_BOX);
                    decrementFiguresCount(player, b);

                    b.resetDodgeCount();
                    isDodge = false;
                }
                fromX--;
                fromY--;
            }
        }
        if ((fromX - whereX > 0) && (fromY - whereY < 0)) {
            while ((whereX != fromX)) {
                if (b.getPlayerFromFigure(new Point(fromX, fromY)) == player * (-1)) {
                    b.setBox(fromX, fromY, CheckersBoard.EMPTY_BOX);
                    decrementFiguresCount(player, b);
                    //
                    b.resetDodgeCount();
                    isDodge = false;
                }
                fromX--;
                fromY++;
            }
        }
        if ((fromX - whereX < 0) && (fromY - whereY < 0)) {
            while ((whereX != fromX)) {
                if (b.getPlayerFromFigure(new Point(fromX, fromY)) == player * (-1)) {
                    b.setBox(fromX, fromY, CheckersBoard.EMPTY_BOX);
                    decrementFiguresCount(player, b);
                    //
                    b.resetDodgeCount();
                    isDodge = false;
                }
                fromX++;
                fromY++;
            }
        }
        if ((fromX - whereX < 0) && (fromY - whereY > 0)) {
            while ((whereX != fromX)) {
                if (b.getPlayerFromFigure(new Point(fromX, fromY)) == player * (-1)) {
                    b.setBox(fromX, fromY, CheckersBoard.EMPTY_BOX);
                    decrementFiguresCount(player, b);
                    //
                    b.resetDodgeCount();
                    isDodge = false;
                }
                fromX++;
                fromY--;
            }
        }

        if (isDodge) {
            b.incrementDodgeCount();
        }

        return b;
    }

    public static void decrementFiguresCount(int player, CheckersBoard b) {
        if (player == Rules.WHITE_PLAYER) {
            b.decrementBlackFigureCount();
        } else if (player == Rules.BLACK_PLAYER) {
            b.decrementWhiteFigureCount();
        }
    }

    public static void printJump(List<Point> listOfPosition) {
        for (Point p : listOfPosition) {
            System.out.print("(" + ((char) (p.getY() + 65)) + "" + (p.getX() + 1) + ")");
        }
        System.out.println("");
    }

    public static void setPlayers(int whitePlayer, int blackPlayer) {
        Control.WHITE_PLAYER_SETTING = whitePlayer;
        Control.BLACK_PLAYER_SETTING = blackPlayer;
    }

    public static boolean isPlayerComputer(int player) {
        switch (player) {
            case (Rules.WHITE_PLAYER):
                if (Control.WHITE_PLAYER_SETTING == Control.COMPUTER_PLAYER) {
                    return true;
                }
                break;
            case (Rules.BLACK_PLAYER):
                if (Control.BLACK_PLAYER_SETTING == Control.COMPUTER_PLAYER) {
                    return true;
                }
                break;
        }

        return false;
    }

    public static int getWHITE_PLAYER_SETTING() {
        return WHITE_PLAYER_SETTING;
    }

    public static int getBLACK_PLAYER_SETTING() {
        return BLACK_PLAYER_SETTING;
    }

    public static boolean arePlayersInitialize() {
        if (WHITE_PLAYER_SETTING != 0 && BLACK_PLAYER_SETTING != 0) {
            return true;
        }
        return false;
    }

    public static boolean arePlayersComputers() {
        return (WHITE_PLAYER_SETTING == 2 && BLACK_PLAYER_SETTING == 2);
    }
}
