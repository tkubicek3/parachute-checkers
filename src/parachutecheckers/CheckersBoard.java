package parachutecheckers;

import java.awt.Point;

/**
 *
 * @author KubiX
 */
public class CheckersBoard {

    public static final int EMPTY_BOX = 0;
    //
    public static final int PAWN = 1;
    public static final int PARACHUTIST = 2;
    public static final int CHECKERS = 3;
    //
    public static final int WHITE_PAWN = PAWN;
    public static final int WHITE_PARACHUTIST = PARACHUTIST;
    public static final int WHITE_CHECKERS = CHECKERS;
    //
    public static final int BLACK_PAWN = -1 * PAWN;
    public static final int BLACK_PARACHUTIST = -1 * PARACHUTIST;
    public static final int BLACK_CHECKERS = -1 * CHECKERS;
    //
    private final int boardSize = 8;
    //
    private int[][] checkersBoard = new int[8][8];
    //
    private int whiteFigureCount; // počet bílých figurek
    private int blackFigureCount; // počet černých figurek
    //
    private int dodgeCount; //počet uhybu
    //------------------------------------//

    public int[][] getBoard() {
        return checkersBoard;
    }

    public void setDodgeCount(int n) {
        dodgeCount = n;
    }

    public void resetDodgeCount() {
        dodgeCount = 1;
    }

    public void incrementDodgeCount() {
        dodgeCount++;
    }

    public int getDodgeCount() {
        return dodgeCount;
    }

    public void setWhiteFigureCount(int n) {
        whiteFigureCount = n;
    }

    public void setBlackFigureCount(int n) {
        blackFigureCount = n;
    }

    public int getWhiteFigureCount() {
        return whiteFigureCount;
    }

    public int getBlackFigureCount() {
        return blackFigureCount;
    }

    public void decrementWhiteFigureCount() {
        whiteFigureCount--;
    }

    public void decrementBlackFigureCount() {
        blackFigureCount--;
    }

    public CheckersBoard() {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                this.checkersBoard[i][j] = EMPTY_BOX;
            }
        }
    }

    public int[][] setBox(int i, int j, int figure) {
        checkersBoard[i][j] = figure;

        return checkersBoard;
    }

    public int[][] setBox(Point p, int figure) {
        checkersBoard[(int) p.x][p.y] = figure;

        return checkersBoard;
    }

    public int getBox(int i, int j) {
        return checkersBoard[i][j];
    }

    public int getBox(Point p) {
        return checkersBoard[p.x][p.y];
    }

    public void printBoard() {

        System.out.println("");
        System.out.print("     ");

        System.out.print("[A] [B] [C] [D] [E] [F] [G] [H]");

        System.out.println("");
        System.out.println("    ---------------------------------");

        for (int i = 0; i < 8; i++) {
            System.out.print("[" + (i + 1) + "] | ");
            for (int j = 0; j < 8; j++) {
                if (getBox(i, j) == 0) {
                    System.out.print(" ");
                } else {
                    if (getBox(i, j) == WHITE_PAWN) {
                        System.out.print("X");
                    }
                    if (getBox(i, j) == WHITE_PARACHUTIST) {
                        System.out.print("Q");
                    }
                    if (getBox(i, j) == WHITE_CHECKERS) {
                        System.out.print("D");
                    }
                    if (getBox(i, j) == BLACK_PAWN) {
                        System.out.print("x");
                    }
                    if (getBox(i, j) == BLACK_PARACHUTIST) {
                        System.out.print("q");
                    }
                    if (getBox(i, j) == BLACK_CHECKERS) {
                        System.out.print("d");
                    }
                }
                System.out.print(" | ");
            }
            System.out.println("");
            System.out.println("    ---------------------------------");
        }
    }

    public int[][] initializeBoard() {

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 4; j++) {
                checkersBoard[0][j * 2 + 1] = WHITE_PARACHUTIST;
                checkersBoard[1][j * 2] = BLACK_PAWN;
                checkersBoard[2][j * 2 + 1] = BLACK_PAWN;
                checkersBoard[5][j * 2] = WHITE_PAWN;
                checkersBoard[6][j * 2 + 1] = WHITE_PAWN;
                checkersBoard[7][j * 2] = BLACK_PARACHUTIST;
            }
        }

        whiteFigureCount = 12;
        blackFigureCount = 12;

        dodgeCount = 1;
        Rules.setPlayerOnMove(Rules.WHITE_PLAYER);

        return checkersBoard;
    }

    public int[][] inicializujDesku2() {

        checkersBoard[0][1] = WHITE_PARACHUTIST;
        checkersBoard[1][2] = BLACK_PAWN;
        checkersBoard[2][3] = BLACK_PAWN;
        checkersBoard[5][4] = WHITE_PAWN;
        checkersBoard[6][5] = WHITE_PAWN;
        checkersBoard[7][6] = BLACK_CHECKERS;

        whiteFigureCount = 12;
        blackFigureCount = 12;

        dodgeCount = 1;
        Rules.setPlayerOnMove(Rules.WHITE_PLAYER);

        return checkersBoard;
    }

    public boolean isPawn(Point p) {

        if ((getBox(p) == WHITE_PAWN) || (getBox(p) == BLACK_PAWN)) {
            return true;
        }
        return false;
    }

    public boolean isParachutist(Point p) {

        if ((getBox(p) == WHITE_PARACHUTIST) || (getBox(p) == BLACK_PARACHUTIST)) {
            return true;
        }
        return false;
    }

    public boolean isCheckers(Point p) {

        if ((getBox(p) == WHITE_CHECKERS) || (getBox(p) == BLACK_CHECKERS)) {
            return true;
        }
        return false;
    }

    public int getPlayerFromFigure(Point p) {
        if (getBox(p) > 0) {
            return 1;
        }
        if (getBox(p) < 0) {
            return -1;
        }
        return 0;
    }

    public boolean isEdgePosition(Point p) {
        if (p.getX() == 0 || p.getX() == 7) {
            return true;
        }
        return false;
    }

    public int getFigureCount(int player, CheckersBoard b) {
        if (player == Rules.WHITE_PLAYER) {
            return blackFigureCount;
        } else if (player == Rules.BLACK_PLAYER) {
            return whiteFigureCount;
        }
        return 0;
    }

    /**
     *
     * @return
     */
    @Override
    public CheckersBoard clone() {

        CheckersBoard cloneBoard = new CheckersBoard();

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                cloneBoard.setBox(i, j, getBoard()[i][j]);
            }
        }

        cloneBoard.whiteFigureCount = whiteFigureCount;
        cloneBoard.blackFigureCount = blackFigureCount;
        cloneBoard.dodgeCount = dodgeCount;

        return cloneBoard;
    }

    public int getBoardSize() {
        return boardSize;
    }

    public void setBoardForGetMove() {
        setBox(0, 6, whiteFigureCount);
        setBox(1, 1, blackFigureCount);
    }
}
