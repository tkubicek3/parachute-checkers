package parachutecheckers;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

public class Mozek {

    private static final int HODNOTA_PESCE = 25;
    private static final int HODNOTA_PARASUTISTY = 35;
    private static final int HODNOTA_DAMY = 100;
    
    private static WinnerChecking winnerChecking = new  WinnerChecking();

    public static int hodnotaPozice(Point po, int hrac, CheckersBoard d) {

        int hodnota = 0;

        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {
                Point p = new Point(x, y);
                if (d.getPlayerFromFigure(p) == hrac) {
                    if (d.isPawn(p)) {
                        hodnota += Mozek.HODNOTA_PESCE;
                    }
                    if (d.isParachutist(p)) {
                        hodnota += Mozek.HODNOTA_PARASUTISTY;
                    }
                    if (d.isCheckers(p)) {
                        hodnota += Mozek.HODNOTA_DAMY;
                    }
                } else if (d.getPlayerFromFigure(p) == hrac * (-1)) {
                    if (d.isPawn(p)) {
                        hodnota -= Mozek.HODNOTA_PESCE;
                    }
                    if (d.isParachutist(p)) {
                        hodnota -= Mozek.HODNOTA_PARASUTISTY;
                    }
                    if (d.isCheckers(p)) {
                        hodnota -= Mozek.HODNOTA_DAMY;
                    }
                }
            }
        }

        if (po.getX() == 0 || po.getX() == 7) {
            hodnota += 30;
        }

        return hodnota;
    }
    //
    public static final int HLOUBKA = 4;

    public static List<Point> inteligentniVyber(int hrac, int hloubka, CheckersBoard d, int debug) {

        List<Point> mozneTahyOdkud = Rules.kontrolaMoznehoSkokuCiVyhry(true, hrac, d);
        if (mozneTahyOdkud.isEmpty()) {
            mozneTahyOdkud = Rules.kontrolaMoznehoSkokuCiVyhry(false, hrac, d);
        }       

        List<Object> potencionalniNej;
        List<Point> nej = new ArrayList<>();
        Point nejOdkud = new Point();
        Point nejKam = new Point();

        int nejlepsi = Integer.MIN_VALUE + 1;

        for (Point odkud : mozneTahyOdkud) {
            potencionalniNej = nejlepsiTah(odkud, hrac, hloubka, d.clone());
            if (((int) potencionalniNej.get(0)) > nejlepsi) {
                nejlepsi = ((int) potencionalniNej.get(0));
                nejOdkud = odkud;
                nejKam = (Point) potencionalniNej.get(1);
            }
        }

        nej.add(nejOdkud);
        nej.add(nejKam);

        return nej;
    }

    public static List<Object> nejlepsiTah(Point p, int hrac, int hloubka, CheckersBoard d) {

        MozekMinimax a = new MozekMinimax();

        List<Point> kamTahnoutZp_ozice = Rules.generatorTahu(p, d, hrac, false);

        int nejlepsi = Integer.MIN_VALUE + 1;
        int cena;
        int h;
        Point nejlepsiKam = new Point();

        for (Point kam : kamTahnoutZp_ozice) {
            a.pridejDoHistorie(d);
            udelejTah(p, kam, d);
            if (hloubka % 2 == 0) {
                h = 1;
            } else {
                h = -1;
            }
            cena = h * miniMax(kam, hloubka - 1, Rules.getPlayerOnMove(), d);
            d = a.undo();

            if (cena > nejlepsi) {
                nejlepsi = cena;
                nejlepsiKam = kam;
            }
        }
        List<Object> nej = new ArrayList<>();
        nej.add(nejlepsi);
        nej.add(nejlepsiKam);

        return nej;
    }

    public static int miniMax(Point p, int hloubka, int hrac, CheckersBoard d) {
        MozekMinimax b = new MozekMinimax();

        if (winnerChecking.isLoss(d, hrac)) {
            return Integer.MIN_VALUE + 11;
        }
        if (winnerChecking.isWinning(d, hrac)) {
            return Integer.MAX_VALUE - 10;
        }
        if (winnerChecking.isDraw(d)) {
            return 0;
        }
        if (hloubka == 0) {
            return hodnotaPozice(p, hrac, d);

        } else {
            Point odkud = inteligentniVyber(hrac, hloubka, d, 2).get(0);
            List<Point> tahy = Rules.generatorTahu(odkud, d, hrac, false);

            int cena = Integer.MIN_VALUE + 1;

            for (Point kam : tahy) {
                b.pridejDoHistorie(d);
                udelejTah(odkud, kam, d);
                cena = Math.max(cena, -miniMax(kam, hloubka - 1, Rules.getPlayerOnMove(), d));
                b.undo();
            }
            return cena;
        }
    }

    public static CheckersBoard udelejTah(Point odkud, Point kam, CheckersBoard d) {
        int hrac = d.getPlayerFromFigure(odkud);
        int pocetFigurek = d.getFigureCount(hrac, d);

        pohyb(odkud, kam, d); //presune figurku
        Control.deleteFigure(odkud, kam, hrac, d);  //smaze preskocene
        if ((pocetFigurek == d.getFigureCount(hrac, d)) || (Rules.vicenasobnySkok(kam, d, hrac))) { //vicensabny skok - kontrola
            Rules.changeToCheckerOrPawn(kam, hrac, d); // zmena na damu ci na pesce
            Rules.setNextPlayer();
        }
        return d;
    }

    public static CheckersBoard pohyb(Point odkud, Point kam, CheckersBoard d) {

        int hodnota_odkud = d.getBox(odkud.x, odkud.y);

        d.setBox(kam, hodnota_odkud);

        return d;
    }

    public static CheckersBoard smaz(Point odkud, Point kam, int hrac, CheckersBoard d) {

        boolean uhyb = true;
        d.setBox(odkud, CheckersBoard.EMPTY_BOX);

        int x = odkud.x;
        int y = odkud.y;

        if ((odkud.getX() - kam.getX() > 0) && (odkud.getY() - kam.getY() > 0)) {
            while ((kam.getX() != x)) {
                if (d.getPlayerFromFigure(new Point(x, y)) == hrac * (-1)) {
                    d.setBox(x, y, CheckersBoard.EMPTY_BOX);
                    Control.decrementFiguresCount(hrac, d);
                    d.resetDodgeCount();
                    uhyb = false;
                }
                x--;
                y--;
            }
        }
        if ((odkud.getX() - kam.getX() > 0) && (odkud.getY() - kam.getY() < 0)) {
            while ((kam.getX() != x)) {
                if (d.getPlayerFromFigure(new Point(x, y)) == hrac * (-1)) {
                    d.setBox(x, y, CheckersBoard.EMPTY_BOX);
                    Control.decrementFiguresCount(hrac, d);
                    d.resetDodgeCount();
                    uhyb = false;
                }
                x--;
                y++;
            }
        }
        if ((odkud.getX() - kam.getX() < 0) && (odkud.getY() - kam.getY() < 0)) {
            while ((kam.getX() != x)) {
                if (d.getPlayerFromFigure(new Point(x, y)) == hrac * (-1)) {
                    d.setBox(x, y, CheckersBoard.EMPTY_BOX);
                    Control.decrementFiguresCount(hrac, d);
                    d.resetDodgeCount();
                    uhyb = false;
                }
                x++;
                y++;
            }
        }
        if ((odkud.getX() - kam.getX() < 0) && (odkud.getY() - kam.getY() > 0)) {
            while ((kam.getX() != x)) {
                if (d.getPlayerFromFigure(new Point(x, y)) == hrac * (-1)) {
                    d.setBox(x, y, CheckersBoard.EMPTY_BOX);
                    Control.decrementFiguresCount(hrac, d);
                    d.resetDodgeCount();
                    uhyb = false;
                }
                x++;
                y--;
            }
        }

        if (uhyb) {
            d.incrementDodgeCount();
        }

        return d;
    }
}
