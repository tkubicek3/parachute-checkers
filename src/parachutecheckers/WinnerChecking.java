package parachutecheckers;

public class WinnerChecking {

    private int WINNING_PLAYER; //vitezici hrac
    public final int DRAW = 0;  //remize

    public int getWinner() {
        return WINNING_PLAYER;
    }

    public String getWinnerColor() {
        if (WINNING_PLAYER == Rules.WHITE_PLAYER) {
            return "Bílý";
        } else if (WINNING_PLAYER == Rules.BLACK_PLAYER) {
            return "Černý";
        }
        return null;
    }

    public boolean checking(CheckersBoard b) {
        if ((b.getWhiteFigureCount() == 0) || (Rules.kontrolaMoznehoSkokuCiVyhry(false, Rules.WHITE_PLAYER, b).isEmpty())) {
            WINNING_PLAYER = Rules.BLACK_PLAYER;
            return true;
        }

        if ((b.getBlackFigureCount() == 0) || (Rules.kontrolaMoznehoSkokuCiVyhry(false, Rules.BLACK_PLAYER, b).isEmpty())) {
            WINNING_PLAYER = Rules.WHITE_PLAYER;
            return true;
        }
        if (b.getDodgeCount() == Rules.MAX_COUNT_OF_DODGE) {
            WINNING_PLAYER = DRAW;
            return true;
        }

        return false;
    }

    public boolean isWinning(CheckersBoard b, int player) {
        switch (player) {
            case (Rules.WHITE_PLAYER):
                if (b.getBlackFigureCount() == 0) {
                    WINNING_PLAYER = Rules.WHITE_PLAYER;
                    return true;
                }
                if (Rules.kontrolaMoznehoSkokuCiVyhry(false, Rules.BLACK_PLAYER, b).isEmpty()) {
                    WINNING_PLAYER = Rules.WHITE_PLAYER;
                    return true;
                }
                break;
            case (Rules.BLACK_PLAYER):
                if (b.getWhiteFigureCount() == 0) {
                    WINNING_PLAYER = Rules.BLACK_PLAYER;
                    return true;
                }
                if (Rules.kontrolaMoznehoSkokuCiVyhry(false, Rules.WHITE_PLAYER, b).isEmpty()) {
                    WINNING_PLAYER = Rules.BLACK_PLAYER;
                    return true;
                }
                break;
        }
        return false;
    }

    public boolean isLoss(CheckersBoard b, int player) {
        switch (player) {
            case (Rules.BLACK_PLAYER):
                if (b.getBlackFigureCount() == 0) {
                    WINNING_PLAYER = Rules.WHITE_PLAYER;
                    return true;
                }
                if (Rules.kontrolaMoznehoSkokuCiVyhry(false, Rules.BLACK_PLAYER, b).isEmpty()) {
                    WINNING_PLAYER = Rules.WHITE_PLAYER;
                    return true;
                }
                break;

            case (Rules.WHITE_PLAYER):
                if (b.getWhiteFigureCount() == 0) {
                    WINNING_PLAYER = Rules.BLACK_PLAYER;
                    return true;
                }
                if (Rules.kontrolaMoznehoSkokuCiVyhry(false, Rules.WHITE_PLAYER, b).isEmpty()) {
                    WINNING_PLAYER = Rules.BLACK_PLAYER;
                    return true;
                }
                break;
        }
        return false;
    }

    public boolean isDraw(CheckersBoard b) {
        if (b.getDodgeCount() == Rules.MAX_COUNT_OF_DODGE) {
            return true;
        }
        return false;
    }
}
