/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mediator;

import gui.*;
import history.*;
import javax.swing.UIManager;
import parachutecheckers.*;

public class Game {

    private static GUI gUserInterface;
    private static CheckersBoard checkersBoard;
    private static BoardPictures boardPictures;
    private static History history;
    private static WinnerChecking winnerChecking;

    public static void main(String[] args) throws Exception {

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }//vzhled
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        boardPictures = new BoardPictures();
        checkersBoard = new CheckersBoard();
        winnerChecking = new WinnerChecking();
        history = new History();
        gUserInterface = new GUI();
    }

    //Getters
    public static BoardPictures getBoardPictures() {
        return boardPictures;
    }

    public static CheckersBoard getCheckersBoard() {
        return checkersBoard;
    }

    public static GUI getgUserInterface() {
        return gUserInterface;
    }

    public static WinnerChecking getWinnerChecking() {
        return winnerChecking;
    }

    public static History getHistory() {
        return history;
    }
}
