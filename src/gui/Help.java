/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import com.sun.xml.internal.bind.v2.runtime.reflect.Accessor;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.LineBorder;
import mediator.Game;

/**
 *
 * @author KubiX
 */
public class Help extends JFrame {

    private JButton buttonRules = new JButton("Pravidla hry     ");
    private JButton buttonEndGame = new JButton("Cíl hry");
    private JButton buttonParachute = new JButton("Parašutistická dáma     ");
    //
    private JEditorPane textArea = new JEditorPane();
    private JPanel buttonsPanel = new JPanel();
    //
    private BoardPictures boardPictures = Game.getBoardPictures();
    //
    private String rules;
    private String endGame;
    private String parachute;

    public Help() {

        this.setTitle("Nápověda");
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setPreferredSize(new Dimension(600, 700));
        this.setIconImage(boardPictures.getIcon().getImage());
        this.setResizable(false);

        setTexts();
        setComponents();
        setListeners();

        buttonsPanel.setBorder(new LineBorder(Color.BLACK, 1, true));
        buttonsPanel.add(buttonParachute);
        buttonsPanel.add(buttonRules);
        buttonsPanel.add(buttonEndGame);

        this.add(new JScrollPane(textArea), BorderLayout.CENTER);
        this.add(buttonsPanel, BorderLayout.SOUTH);

        this.pack();
    }

    private void setComponents() {
        Font font = new Font("Tahoma", Font.BOLD, 18);
        Color color = Color.BLACK;

        buttonParachute.setContentAreaFilled(false);
        buttonParachute.setBorder(null);
        buttonParachute.setFont(font);
        buttonParachute.setForeground(color);
        buttonParachute.setFocusPainted(false);

        buttonEndGame.setContentAreaFilled(false);
        buttonEndGame.setBorder(null);
        buttonEndGame.setFont(font);
        buttonEndGame.setForeground(color);
        buttonEndGame.setFocusPainted(false);

        buttonRules.setContentAreaFilled(false);
        buttonRules.setBorder(null);
        buttonRules.setFont(font);
        buttonRules.setForeground(color);
        buttonRules.setFocusPainted(false);

        textArea.setEditable(false);
        textArea.setContentType("text/html");
        textArea.setText(parachute);
    }

    private void setListeners() {
        buttonParachute.addActionListener(new ClickToParachute());
        buttonEndGame.addActionListener(new ClickToEndGame());
        buttonRules.addActionListener(new ClickToRules());
    }

    private void setTexts() {
        parachute = getHtmlFromFile("aaa.html");
        rules = "<html><h1 style = \"text-align: center\">Pravidla hry</h1></html>";
        endGame = "<html><h1 style = \"text-align: center\">Cíl hry</h1></html>";
    }

    private static String getHtmlFromFile(String file) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            try (BufferedReader inputFile = new BufferedReader(new FileReader(file))) {
                String string;
                while ((string = inputFile.readLine()) != null) {
                    stringBuilder.append(string);
                }
            }
        } catch (IOException e) {
        }
        stringBuilder.replace(0, 1, "");
       
        return stringBuilder.toString();
    }

    @Override
    public void setVisible(boolean visible) {
        this.setLocationRelativeTo(Game.getgUserInterface());

        super.setVisible(visible);
    }

    //Clicks
    private class ClickToParachute implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            textArea.setText(parachute);
        }
    }

    private class ClickToEndGame implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            textArea.setText(endGame);
        }
    }

    private class ClickToRules implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            textArea.setText(rules);
        }
    }
}
