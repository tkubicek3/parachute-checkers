package gui;

import mediator.Game;

public class State {

    private CheckersBox fromPosition;
    private CheckersBox toPosition;
    //
    private boolean isFromPositionInitialize;
    private boolean isToPositionInitialize;
    //
    private BoardPictures boardPictures;

    public State() {
        fromPosition = new CheckersBox();
        toPosition = new CheckersBox();
        isFromPositionInitialize = false;
        isToPositionInitialize = false;
        boardPictures = Game.getBoardPictures();
    }

    public void initializeFromPosition(CheckersBox from) {
        this.fromPosition = from;
        this.isFromPositionInitialize = true;
        this.fromPosition.setIsMarked(true);
        this.fromPosition.setIcon(fromPosition.getExitMarkedIcon());
    }

    public void initializeToPosition(CheckersBox to) {
        this.toPosition = to;
        this.isToPositionInitialize = true;
        this.toPosition.setIsMarked(true);
        this.toPosition.setIcon(toPosition.getExitMarkedIcon());
    }

    public void deinitializeState() {
        isFromPositionInitialize = false;
        isToPositionInitialize = false;

        fromPosition.setIsMarked(false);
        toPosition.setIsMarked(false);

        this.fromPosition.setIcon(fromPosition.getExitIcon());
        this.toPosition.setIcon(toPosition.getExitIcon());
    }

    public boolean isFromPositionInitialize() {
        return isFromPositionInitialize;
    }

    public boolean isToPositionInitialize() {
        return isToPositionInitialize;
    }

    public CheckersBox getFromPosition() {
        return fromPosition;
    }

    public CheckersBox getToPosition() {
        return toPosition;
    }

    public int getFromX() {
        return (int) fromPosition.getCoordinate().x;
    }

    public int getFromY() {
        return (int) fromPosition.getCoordinate().y;
    }

    public int getToX() {
        return (int) toPosition.getCoordinate().x;
    }

    public int getToY() {
        return (int) toPosition.getCoordinate().y;
    }
}
