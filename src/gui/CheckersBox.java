package gui;

import java.awt.Point;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import mediator.Game;
import parachutecheckers.*;

public class CheckersBox extends JButton {

    private BoardPictures boardPictures;
    private Point coordinate;
    private int boxValue;
    private boolean marked;
    private boolean clickable = false;
    //
    private ImageIcon exitIcon;
    private ImageIcon enteredIcon;
    private ImageIcon exitMarkedIcon;
    private ImageIcon enteredMarkedIcon;

    public CheckersBox() {
        this.boardPictures = Game.getBoardPictures();
        this.boxValue = CheckersBoard.EMPTY_BOX;
        this.setBorder(null);
        this.setFocusPainted(false);
        setIcons();
    }

    public CheckersBox(ImageIcon imageIcon) {
        this.boardPictures = Game.getBoardPictures();
        this.boxValue = CheckersBoard.EMPTY_BOX;
        setIcons();
        this.setIcon(imageIcon);
        this.setBorder(null);
        this.setFocusPainted(false);
    }

    public CheckersBox(Point coordinate) {
        this.boardPictures = Game.getBoardPictures();
        this.coordinate = coordinate;
        this.boxValue = CheckersBoard.EMPTY_BOX;
        setIcons();
        this.setBorder(null);
        this.setFocusPainted(false);
    }

    @Override
    public CheckersBox clone() {
        CheckersBox checkersBox = new CheckersBox();
        checkersBox.setIcon(this.getIcon());
        checkersBox.setBorder(null);
        this.setBorder(null);
        this.setFocusPainted(false);

        return checkersBox;
    }

    public Point getCoordinate() {
        return coordinate;
    }

    public int getBoxValue() {
        return boxValue;
    }

    public void setBoxValue(int boxValue) {
        this.boxValue = boxValue;
        setIcons();
    }

    public boolean isMarked() {
        return marked;
    }

    public void setIsMarked(boolean isMarked) {
        this.marked = isMarked;
    }

    public ImageIcon getExitIcon() {
        return exitIcon;
    }

    public ImageIcon getEnteredIcon() {
        return enteredIcon;
    }

    public ImageIcon getExitMarkedIcon() {
        return exitMarkedIcon;
    }

    public ImageIcon getEnteredMarkedIcon() {
        return enteredMarkedIcon;
    }

    public void setClickable(boolean clickable) {
        this.clickable = clickable;
    }

    public boolean isClickable() {
        return clickable;
    }
    
    

    private void setIcons() {
        switch (boxValue) {
            case (CheckersBoard.EMPTY_BOX):
                this.exitIcon = boardPictures.getEmptyBox();
                this.enteredIcon = boardPictures.getMouseEnteredEmptyBox();
                this.exitMarkedIcon = null;
                this.enteredMarkedIcon = null;
                break;
            case (CheckersBoard.WHITE_PAWN):
                this.exitIcon = boardPictures.getWhitePawn();
                this.enteredIcon = boardPictures.getMouseEnteredWhitePawn();
                this.exitMarkedIcon = boardPictures.getMarkedWhitePawn();
                this.enteredMarkedIcon = boardPictures.getMarkedMouseEnteredWhitePawn();
                break;
            case (CheckersBoard.WHITE_PARACHUTIST):
                this.exitIcon = boardPictures.getWhiteParachutist();
                this.enteredIcon = boardPictures.getMouseEnteredWhiteParachutist();
                this.exitMarkedIcon = boardPictures.getMarkedWhiteParachutist();
                this.enteredMarkedIcon = boardPictures.getMarkedMouseEnteredWhiteParachutist();
                break;
            case (CheckersBoard.WHITE_CHECKERS):
                this.exitIcon = boardPictures.getWhiteCheckers();
                this.enteredIcon = boardPictures.getMouseEnteredWhiteCheckers();
                this.exitMarkedIcon = boardPictures.getMarkedWhiteCheckers();
                this.enteredMarkedIcon = boardPictures.getMarkedMouseEnteredWhiteCheckers();
                break;
            case (CheckersBoard.BLACK_PAWN):
                this.exitIcon = boardPictures.getBlackPawn();
                this.enteredIcon = boardPictures.getMouseEnteredBlackPawn();
                this.exitMarkedIcon = boardPictures.getMarkedBlackPawn();
                this.enteredMarkedIcon = boardPictures.getMarkedMouseEnteredBlackPawn();
                break;
            case (CheckersBoard.BLACK_PARACHUTIST):
                this.exitIcon = boardPictures.getBlackParachutist();
                this.enteredIcon = boardPictures.getMouseEnteredBlackParachutist();
                this.exitMarkedIcon = boardPictures.getMarkedBlackParachutist();
                this.enteredMarkedIcon = boardPictures.getMarkedMouseEnteredBlackParachutist();
                break;
            case (CheckersBoard.BLACK_CHECKERS):
                this.exitIcon = boardPictures.getBlackCheckers();
                this.enteredIcon = boardPictures.getMouseEnteredBlackCheckers();
                this.exitMarkedIcon = boardPictures.getMarkedBlackCheckers();
                this.enteredMarkedIcon = boardPictures.getMarkedMouseEnteredBlackCheckers();
                break;
        }
    }
}
