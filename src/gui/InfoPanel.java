package gui;

import java.awt.Graphics;
import java.awt.List;
import java.awt.Point;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import mediator.Game;
import parachutecheckers.Rules;

public class InfoPanel extends JPanel {

    private BoardPictures boardPictures = Game.getBoardPictures();

    public InfoPanel() {
        initComponents();

        this.labelPlayerOnMove.setIcon(boardPictures.getWelcome());
        this.labelLogo.setIcon(boardPictures.getLogo());
    }

    @Override
    protected void paintComponent(Graphics grphcs) {
        super.paintComponent(grphcs);
        grphcs.drawImage(boardPictures.getBackgroundImageForInfoPanel().getImage(), 0, 0, null);
    }

    public JButton getButtonRedo() {
        return buttonRedo;
    }

    public JButton getButtonUndo() {
        return buttonUndo;
    }

    public JButton getButtonComputersMove() {
        return buttonComputerMove;
    }

    public JLabel getLabelPlayerOnMove() {
        return labelPlayerOnMove;
    }

    public int getMove() {
        return move;
    }

    public void setMove(int move) {
        this.move = move;
    }

    public List getTextAreaMovesPrinter() {
        return textAreaMovesPrinter;
    }
    private int move = 0;

    public void addMoveToTextArea(String moveText) {
        for (int i = textAreaMovesPrinter.getItemCount() - 1; i > textAreaMovesPrinter.getSelectedIndex(); i--) {
            textAreaMovesPrinter.remove(i);
        }
        move++;
        this.textAreaMovesPrinter.add(move + ": " + moveText + "\n");
        this.textAreaMovesPrinter.select(move);
    }

    public void undoMoveInTextArea() {
        move--;
        this.textAreaMovesPrinter.select(move);
    }

    public void redoMoveInTextArea() {
        move++;
        this.textAreaMovesPrinter.select(move);
    }

    public void reInitializeTextArea() {
        this.move = 0;
        this.textAreaMovesPrinter.removeAll();
        this.textAreaMovesPrinter.add("                  Vítejte v nové hře");
        this.textAreaMovesPrinter.select(0);
    }

    public void setCoordinateLabel(Point coordinate) {
        if (coordinate.getX() != -1) {
            this.labelCoordinate.setText(convertNumberToLetter((int) coordinate.getX()) + "" + (8 - (int) coordinate.getY()));
        } else {
            this.labelCoordinate.setText("...");
        }
    }

    public void setLabelInformation(String text) {
        this.labelInformation.setText(text);
    }

    private String convertNumberToLetter(int number) {
        switch (number) {
            case (0):
                return "A";
            case (1):
                return "B";
            case (2):
                return "C";
            case (3):
                return "D";
            case (4):
                return "E";
            case (5):
                return "F";
            case (6):
                return "G";
            case (7):
                return "H";
            default:
                return null;
        }
    }

    public void setPanelForNewGame() {
        this.buttonComputerMove.setEnabled(false);
        this.buttonUndo.setEnabled(false);
        this.buttonRedo.setEnabled(false);
    }

    public void setLabelPlayerOnMove() {
        if (Rules.getPlayerOnMove() == Rules.WHITE_PLAYER) {
            this.labelPlayerOnMove.setIcon(boardPictures.getWhiteIsOnMove());
        } else if (Rules.getPlayerOnMove() == Rules.BLACK_PLAYER) {
            this.labelPlayerOnMove.setIcon(boardPictures.getBlackIsOnMove());
        } else {
            this.labelPlayerOnMove.setIcon(boardPictures.getWelcome());
        }
    }

    public void setLabelPlayerWinner(int player) {
        if (player == Rules.WHITE_PLAYER) {
            this.labelPlayerOnMove.setIcon(boardPictures.getWhiteIsWinner());
        } else if (player == Rules.BLACK_PLAYER) {
            this.labelPlayerOnMove.setIcon(boardPictures.getBlackIsWinner());
        } else {
            this.labelPlayerOnMove.setIcon(boardPictures.getWelcome());
        }
    }

    public void setInfomationLabel(String text) {
        setLabelInformation(text);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelLogo = new javax.swing.JLabel();
        labelPlayerOnMove = new javax.swing.JLabel();
        buttonComputerMove = new javax.swing.JButton();
        buttonUndo = new javax.swing.JButton();
        buttonRedo = new javax.swing.JButton();
        labelCoordinate = new javax.swing.JLabel();
        labelInformation = new javax.swing.JLabel();
        textAreaMovesPrinter = new java.awt.List();

        setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        setOpaque(false);

        labelLogo.setFont(new java.awt.Font("Tekton Pro", 1, 24)); // NOI18N
        labelLogo.setForeground(new java.awt.Color(255, 255, 255));
        labelLogo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        labelPlayerOnMove.setFont(new java.awt.Font("Tekton Pro", 1, 18)); // NOI18N

        buttonComputerMove.setText("Tah počítače");
        buttonComputerMove.setEnabled(false);
        buttonComputerMove.setFocusPainted(false);
        buttonComputerMove.setOpaque(false);
        buttonComputerMove.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                buttonComputerMoveMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                buttonComputerMoveMouseExited(evt);
            }
        });
        buttonComputerMove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonComputerMoveActionPerformed(evt);
            }
        });

        buttonUndo.setText("Tah zpět");
        buttonUndo.setEnabled(false);
        buttonUndo.setFocusPainted(false);
        buttonUndo.setOpaque(false);
        buttonUndo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                buttonUndoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                buttonUndoMouseExited(evt);
            }
        });
        buttonUndo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonUndoActionPerformed(evt);
            }
        });

        buttonRedo.setText("Tah vpřed");
        buttonRedo.setToolTipText("");
        buttonRedo.setEnabled(false);
        buttonRedo.setFocusPainted(false);
        buttonRedo.setOpaque(false);
        buttonRedo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                buttonRedoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                buttonRedoMouseExited(evt);
            }
        });
        buttonRedo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonRedoActionPerformed(evt);
            }
        });

        labelCoordinate.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        labelCoordinate.setForeground(new java.awt.Color(255, 255, 255));
        labelCoordinate.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelCoordinate.setText("...");
        labelCoordinate.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 255), 5, true));

        labelInformation.setBackground(new java.awt.Color(0, 0, 0));
        labelInformation.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        labelInformation.setForeground(new java.awt.Color(255, 255, 255));
        labelInformation.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelInformation.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 255), 5, true));

        textAreaMovesPrinter.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        textAreaMovesPrinter.setName(""); // NOI18N
        textAreaMovesPrinter.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                textAreaMovesPrinterMouseClicked(evt);
            }
        });
        textAreaMovesPrinter.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                textAreaMovesPrinterMouseDragged(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(textAreaMovesPrinter, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(buttonUndo, javax.swing.GroupLayout.DEFAULT_SIZE, 129, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonRedo, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE))
                    .addComponent(buttonComputerMove, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(labelPlayerOnMove, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(labelLogo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(labelCoordinate, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelInformation, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelLogo, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelPlayerOnMove, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(labelInformation, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(labelCoordinate, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE))
                .addGap(13, 13, 13)
                .addComponent(textAreaMovesPrinter, javax.swing.GroupLayout.DEFAULT_SIZE, 188, Short.MAX_VALUE)
                .addGap(13, 13, 13)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(buttonUndo, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
                    .addComponent(buttonRedo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(buttonComputerMove, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void buttonComputerMoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonComputerMoveActionPerformed
        if (buttonComputerMove.isEnabled()) {
            Game.getgUserInterface().getGraphicCheckersBoard().computerMove();
            buttonComputerMove.setEnabled(Game.getgUserInterface().getGraphicCheckersBoard().isPlayerOnMoveComputer());

            if (!Game.getgUserInterface().getGraphicCheckersBoard().isPlayerOnMoveComputer()) {
                Game.getgUserInterface().getGraphicCheckersBoard().enableClicksOnBoard();
            }

            if (Game.getgUserInterface().getGraphicCheckersBoard().winnerChecking()) {
                Game.getgUserInterface().getGraphicCheckersBoard().winnerAction();
            }
        }
    }//GEN-LAST:event_buttonComputerMoveActionPerformed

    private void buttonUndoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonUndoActionPerformed
        Game.getgUserInterface().getGraphicCheckersBoard().undoAction();
        buttonUndoMouseEntered(null);
    }//GEN-LAST:event_buttonUndoActionPerformed

    private void buttonRedoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonRedoActionPerformed
        Game.getgUserInterface().getGraphicCheckersBoard().redoAction();
        buttonRedoMouseEntered(null);
    }//GEN-LAST:event_buttonRedoActionPerformed

    private void buttonUndoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_buttonUndoMouseEntered
        if (buttonUndo.isEnabled()) {
            setInfomationLabel("Klikni pro tah zpět");
        } else if (!Game.getgUserInterface().getGraphicCheckersBoard().isEmpty()) {
            setInfomationLabel("Nyní nelze provést tah zpět");
        }
    }//GEN-LAST:event_buttonUndoMouseEntered

    private void buttonUndoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_buttonUndoMouseExited
        setInfomationLabel("");
    }//GEN-LAST:event_buttonUndoMouseExited

    private void buttonRedoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_buttonRedoMouseEntered
        if (buttonRedo.isEnabled()) {
            setInfomationLabel("Klikni pro tah vpřed");
        } else if (!Game.getgUserInterface().getGraphicCheckersBoard().isEmpty()) {
            setInfomationLabel("Nyní nelze provést tah vpřed");
        }
    }//GEN-LAST:event_buttonRedoMouseEntered

    private void buttonRedoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_buttonRedoMouseExited
        setInfomationLabel("");
    }//GEN-LAST:event_buttonRedoMouseExited

    private void buttonComputerMoveMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_buttonComputerMoveMouseEntered
        if (buttonComputerMove.isEnabled()) {
            setInfomationLabel("Klikni pro tah počítače");
        } else if (!Game.getgUserInterface().getGraphicCheckersBoard().isEmpty()) {
            setInfomationLabel("Nyní není dostupné");
        }
    }//GEN-LAST:event_buttonComputerMoveMouseEntered

    private void buttonComputerMoveMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_buttonComputerMoveMouseExited
        setInfomationLabel("");
    }//GEN-LAST:event_buttonComputerMoveMouseExited

    private void textAreaMovesPrinterMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_textAreaMovesPrinterMouseClicked
        int historyMoves = textAreaMovesPrinter.getSelectedIndex() - move;
        int selectedIndex = textAreaMovesPrinter.getSelectedIndex();

        if (historyMoves < 0) {
            for (int i = historyMoves; i < 0; i++) {
                Game.getgUserInterface().getGraphicCheckersBoard().undoAction();
            }
        } else if (historyMoves > 0) {
            for (int i = historyMoves; i > 0; i--) {
                Game.getgUserInterface().getGraphicCheckersBoard().redoAction();
            }
        }
        textAreaMovesPrinter.select(selectedIndex);
    }//GEN-LAST:event_textAreaMovesPrinterMouseClicked

    private void textAreaMovesPrinterMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_textAreaMovesPrinterMouseDragged
        textAreaMovesPrinterMouseClicked(evt);
    }//GEN-LAST:event_textAreaMovesPrinterMouseDragged
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonComputerMove;
    private javax.swing.JButton buttonRedo;
    private javax.swing.JButton buttonUndo;
    private javax.swing.JLabel labelCoordinate;
    private javax.swing.JLabel labelInformation;
    private javax.swing.JLabel labelLogo;
    private javax.swing.JLabel labelPlayerOnMove;
    private java.awt.List textAreaMovesPrinter;
    // End of variables declaration//GEN-END:variables
}
