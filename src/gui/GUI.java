package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import mediator.Game;
import history.*;

/**
 *
 * @author KubiX
 */
public class GUI extends JFrame {

    //
    private GraphicCheckersBoard graphicCheckersBoard = new GraphicCheckersBoard();
    private BoardPictures boardPictures = Game.getBoardPictures();
    //
    private JMenuBar barMenu = new JMenuBar();                                  //<barMenu>//
    //
    private JMenu menuFile = new JMenu("Soubor");                                   //<menuFile> 
    private JMenuItem menuItemNewGame = new JMenuItem("Nová hra");
    private JMenuItem menuItemLoadGame = new JMenuItem("Načíst hru");
    private JMenuItem menuItemSaveGame = new JMenuItem("Uložit hru");
    private JPopupMenu.Separator separatorMenuFile1 = new JPopupMenu.Separator();
    private JPopupMenu.Separator separatorMenuFile2 = new JPopupMenu.Separator();
    private JMenuItem menuItemExitFile = new JMenuItem("Konec");                    //</ menuFile> 
    //
    private JMenu menuEdit = new JMenu("Volby");                                    //<menuEdit>
    private JMenuItem menuItemUndo = new JMenuItem("Tah zpět");
    private JMenuItem menuItemRedo = new JMenuItem("Tah vpřed");                     //</ menuEdit>
    //
    private JMenu menuOptions = new JMenu("Možnosti");                               //<menuTools>
    private JMenuItem menuItemSetPlayers = new JMenuItem("Nastavení hráčů");          //</ menuTools>
    //
    private JMenu menuHelp = new JMenu("Nápověda");
    private JMenuItem menuItemHelp = new JMenuItem("Nápověda");
    private JMenuItem menuItemAboutApp = new JMenuItem("O aplikaci");
    //
    //                                                                          //</barMenu>//
    //
    //
    private InfoPanel infoPanel;
    private JFileChooser fileChooser = new JFileChooser();
    private Help help = new Help();
    //
    private boolean needSave = false;
    private ExitOrSaveDialog exitOrSaveDialog = new ExitOrSaveDialog(this, true);
    private SetPlayersDialog setPlayersDialog = new SetPlayersDialog();
    //
    private int height = boardPictures.getBoxSize() * 10;
    private int width = height + 240;

    public GUI() {

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName()); //vzhled
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            System.out.println(e.getMessage());
        }

        this.infoPanel = new InfoPanel();

        this.setTitle("Parašutistická dáma LS/2013");
        this.setIconImage(boardPictures.getIcon().getImage());
        this.addWindowListener(new ExitActionFromWindow());
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        this.setMinimumSize(new Dimension(width, height));

        this.setMnemonics();
        this.initializeMenus();
        this.setMenuItemEvents();

        this.setJMenuBar(barMenu);
        this.add(graphicCheckersBoard, BorderLayout.WEST);
        this.add(infoPanel, BorderLayout.CENTER);

        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setVisible(true);

        this.pack();
    }
    //
    //
    //
    //

    private void setMnemonics() {
        this.menuFile.setMnemonic(KeyEvent.VK_S);
        this.menuItemExitFile.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, ActionEvent.ALT_MASK));
        this.menuItemNewGame.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
        this.menuItemLoadGame.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
        this.menuItemSaveGame.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
        //
        this.menuEdit.setMnemonic(KeyEvent.VK_V);
        this.menuItemUndo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, ActionEvent.CTRL_MASK));
        this.menuItemRedo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Y, ActionEvent.CTRL_MASK));
        //
        this.menuOptions.setMnemonic(KeyEvent.VK_M);
        this.menuItemSetPlayers.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F12, 0));
        //
        this.menuHelp.setMnemonic(KeyEvent.VK_N);
        this.menuItemHelp.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
        this.menuItemAboutApp.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F10, 0));
    }

    private void initializeMenus() {
        this.menuFile.add(menuItemNewGame);
        this.menuFile.add(separatorMenuFile1);
        this.menuFile.add(menuItemLoadGame);
        this.menuFile.add(menuItemSaveGame);
        this.menuFile.add(separatorMenuFile2);
        this.menuFile.add(menuItemExitFile);
        this.barMenu.add(menuFile);
        //
        this.menuEdit.add(menuItemUndo);
        this.menuEdit.add(menuItemRedo);
        this.barMenu.add(menuEdit);
        //
        this.menuOptions.add(menuItemSetPlayers);
        this.barMenu.add(menuOptions);
        //
        this.menuEdit.setEnabled(false);
        this.menuItemUndo.setEnabled(false);
        this.menuItemRedo.setEnabled(false);

        this.menuOptions.setEnabled(false);
        this.menuItemSetPlayers.setEnabled(true);

        this.menuItemSaveGame.setEnabled(false);
        //
        this.menuHelp.add(menuItemHelp);
        this.menuHelp.add(menuItemAboutApp);
        this.barMenu.add(menuHelp);
        //Icons
        this.menuItemNewGame.setIcon(boardPictures.getNewGameIcon());
        this.menuItemLoadGame.setIcon(boardPictures.getLoadIcon());
        this.menuItemSaveGame.setIcon(boardPictures.getSaveIcon());
        this.menuItemExitFile.setIcon(boardPictures.getExitIcon());
        this.menuItemUndo.setIcon(boardPictures.getUndoIcon());
        this.menuItemRedo.setIcon(boardPictures.getRedoIcon());
        this.menuItemSetPlayers.setIcon(boardPictures.getSettingsIcon());
    }

    public void setMenusItemInGame() {

        this.menuItemSaveGame.setEnabled(!graphicCheckersBoard.isEmpty());
        //
        this.menuOptions.setEnabled(!graphicCheckersBoard.isEmpty());
        this.menuItemSetPlayers.setEnabled(!graphicCheckersBoard.isEmpty());
        //
        this.infoPanel.getButtonUndo().setEnabled(Game.getHistory().isPossibleUndoMove());
        this.menuItemUndo.setEnabled(Game.getHistory().isPossibleUndoMove());
        //
        this.infoPanel.getButtonRedo().setEnabled(Game.getHistory().isPossibleRedoMove());
        this.menuItemRedo.setEnabled(Game.getHistory().isPossibleRedoMove());
        //
        this.menuEdit.setEnabled(infoPanel.getButtonUndo().isEnabled() || infoPanel.getButtonRedo().isEnabled());
    }

    public void setMenusAndButtonsAfterWinning() {
        infoPanel.getButtonComputersMove().setEnabled(false);
        infoPanel.getButtonRedo().setEnabled(false);
        this.menuItemRedo.setEnabled(false);
        this.menuItemSetPlayers.setEnabled(false);
        this.menuOptions.setEnabled(false);
    }

    // <Events>
    private class ExitActionFromWindow extends WindowAdapter {

        @Override
        public void windowClosing(WindowEvent we) {
            if (!graphicCheckersBoard.isEmpty() && needSave) {
                exitOrSaveDialog.setVisibled(true);
                saveBeforeExitAction(exitOrSaveDialog.getSaveOrExit());
            } else {
                setPlayersDialog.dispose();
                help.dispose();
                setVisible(false);
                dispose();
            }
        }
    }

    private void setMenuItemEvents() {
        this.menuItemExitFile.addActionListener(new ExitAction());
        this.menuItemNewGame.addActionListener(new NewGameAction());
        this.menuItemLoadGame.addActionListener(new LoadGameAction());
        this.menuItemSaveGame.addActionListener(new SaveGameAction());
        //
        this.menuItemUndo.addActionListener(new UndoAction());
        this.menuItemRedo.addActionListener(new RedoAction());
        //
        this.menuItemSetPlayers.addActionListener(new SetPlayersAction());
        //
        this.menuItemHelp.addActionListener(new HelpAction());
        this.menuItemAboutApp.addActionListener(new AboutAppAction());
    }

    private class ExitAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            if (!graphicCheckersBoard.isEmpty() && needSave) {
                exitOrSaveDialog.setVisibled(true);
                saveBeforeExitAction(exitOrSaveDialog.getSaveOrExit());
            } else {
                setPlayersDialog.dispose();
                help.dispose();
                setVisible(false);
                dispose();
            }
        }
    }

    private void saveBeforeExitAction(int saveOrExit) {
        switch (saveOrExit) {
            case (ExitOrSaveDialog.SAVE):
                SaveGameAction saveGameAction = new SaveGameAction();
                saveGameAction.actionPerformed(null);
                //
                if (saveGameAction.isChooseFile) {
                    setPlayersDialog.dispose();
                    setVisible(false);
                    dispose();
                }
                break;
            case (ExitOrSaveDialog.EXIT):
                setPlayersDialog.dispose();
                setVisible(false);
                dispose();
                break;
            case (ExitOrSaveDialog.STORNO):
                break;
        }
    }

    private class NewGameAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            setPlayersDialog.setVisibled(true);

            if (!setPlayersDialog.isCanceled()) {
                graphicCheckersBoard.setBoardForNewGame(setPlayersDialog.getWhiteSetting(), setPlayersDialog.getBlackSetting());
                setMenusItemInGame();
            }
        }
    }

    private class LoadGameAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            int value = fileChooser.showOpenDialog(Game.getgUserInterface());

            if (value == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
                graphicCheckersBoard.loadAction(file.getAbsolutePath());
            }

            setMenusItemInGame();
        }
    }

    private class SaveGameAction implements ActionListener {

        private boolean isChooseFile = false;

        public boolean isIsChooseFile() {
            return isChooseFile;
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            int value = fileChooser.showSaveDialog(Game.getgUserInterface());

            if (value == JFileChooser.APPROVE_OPTION) {
                isChooseFile = true;
                File file = fileChooser.getSelectedFile();
                graphicCheckersBoard.saveAction(file.getAbsolutePath());
            }
        }
    }

    private class UndoAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            graphicCheckersBoard.undoAction();
        }
    }

    private class RedoAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            try {
                graphicCheckersBoard.redoAction();
            } catch (Exception ex) {
                Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private class SetPlayersAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            setPlayersDialog.setVisibled(true);
            if (!setPlayersDialog.isCanceled()) {
                graphicCheckersBoard.setPlayers(setPlayersDialog.getWhiteSetting(), setPlayersDialog.getBlackSetting());
                if (graphicCheckersBoard.isPlayerOnMoveComputer() || graphicCheckersBoard.arePlayersComputers()) {
                    infoPanel.getButtonComputersMove().setEnabled(true);
                    graphicCheckersBoard.disableClicksOnBoard();
                } else {
                    infoPanel.getButtonComputersMove().setEnabled(false);
                    graphicCheckersBoard.enableClicksOnBoard();
                }
            }
        }
    }
    // </ Events>

    //< Getters And Setters>
    public InfoPanel getInfoPanel() {
        return infoPanel;
    }

    public GraphicCheckersBoard getGraphicCheckersBoard() {
        return graphicCheckersBoard;
    }

    public boolean getNeedSave() {
        return needSave;
    }

    public void setNeedSave(boolean needSave) {
        this.needSave = needSave;
    }

    public JMenu getMenuOptions() {
        return menuOptions;
    }

    public JMenuItem getMenuItemSetPlayers() {
        return menuItemSetPlayers;
    }

    public SetPlayersDialog getSetPlayersDialog() {
        return setPlayersDialog;
    }

    private class HelpAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            help.setVisible(true);
        }
    }

    private class AboutAppAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }
}
