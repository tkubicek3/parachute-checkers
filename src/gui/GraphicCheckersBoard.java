/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import history.OperationsWithCheckersBoard;
import history.SaveAndLoad;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;
import java.util.List;
import javax.swing.*;
import javax.swing.border.LineBorder;
import mediator.Game;
import parachutecheckers.*;

/**
 *
 * @author KubiX
 */
public class GraphicCheckersBoard extends JPanel {

    private static final int boardSize = Game.getCheckersBoard().getBoardSize();
    //
    private CheckersBoard checkersBoard = Game.getCheckersBoard();
    private CheckersBox[][] gCheckersBoard = new CheckersBox[8][8];
    private BoardPictures boardPictures = Game.getBoardPictures();
    private WinnerChecking winnerChecking = Game.getWinnerChecking();
    private State state = new State();
    //
    private JPanel gamePanel = new JPanel(new GridBagLayout());

    public GraphicCheckersBoard() {

        this.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));

        initializeGraphicsBoard();

        this.add(gamePanel);
    }

    private void initializeGraphicsBoard() {

        int num = 0;
        GridBagConstraints position = new GridBagConstraints();

        for (int x = 0; x < boardSize; x++) {
            for (int y = 0; y < boardSize; y++) {

                gCheckersBoard[x][y] = new CheckersBox(new Point(x, y));

                if (num % 2 != 0) {
                    gCheckersBoard[x][y].setIcon(boardPictures.getEmptyBox());
                    initializeMouseAction(gCheckersBoard[x][y]);
                } else {
                    gCheckersBoard[x][y].setIcon(boardPictures.getNullBox());
                }

                if (y == 7) {
                    num++;
                }
                num++;

                position.gridx = x + 1;
                position.gridy = y + 1;

                this.gamePanel.add(gCheckersBoard[x][y], position);
            }
        }

        setLettersAndNumbers();
        setCorners();
    }

    public void rePaint() {
        int num = 0;

        for (int x = 0; x < boardSize; x++) {
            for (int y = 0; y < boardSize; y++) {
                if (num % 2 != 0) {
                    gCheckersBoard[x][y].setBoxValue(checkersBoard.getBox(y, x));
                    setCheckersBoxIcon(gCheckersBoard[x][y]);
                }
                if (y == 7) {
                    num++;
                }
                num++;
            }
        }

        //checkersBoard.printBoard(); //Debug

        Game.getgUserInterface().setNeedSave(true);
        Game.getgUserInterface().setMenusItemInGame();
        //
        Game.getgUserInterface().getInfoPanel().setLabelPlayerOnMove();
        //
    }

    public void setBoardForNewGame(int whiteSetting, int blackSetting) {

        checkersBoard = Game.getCheckersBoard();
        checkersBoard = new CheckersBoard();
        checkersBoard.initializeBoard();

        Game.getgUserInterface().getInfoPanel().setPanelForNewGame();
        state.deinitializeState();

        int num = 0;

        for (int x = 0; x < boardSize; x++) {
            for (int y = 0; y < boardSize; y++) {

                if (num % 2 != 0) {
                    gCheckersBoard[x][y].setBoxValue(checkersBoard.getBox(y, x));
                    setCheckersBoxIcon(gCheckersBoard[x][y]);
                }
                if (y == 7) {
                    num++;
                }
                num++;
            }
        }

        rePaint();

        Game.getHistory().reInitialize();
        //
        enableClicksOnBoard();
        setPlayers(whiteSetting, blackSetting);
        Game.getgUserInterface().getInfoPanel().reInitializeTextArea();
        //

        if (isPlayerOnMoveComputer()) {
            computerMove();
        }
        if (arePlayersComputers()) {
            setBoardForPcVersusPcGame();
        }
        //
        Game.getgUserInterface().getInfoPanel().setLabelPlayerOnMove();
    }

    public boolean arePlayersComputers() {
        return Control.arePlayersComputers();
    }

    public void setBoardForPcVersusPcGame() {
        Game.getgUserInterface().getInfoPanel().getButtonComputersMove().setEnabled(true);
        disableClicksOnBoard();
    }

    public boolean isPlayerOnMoveComputer() {
        return Control.isPlayerComputer(Rules.getPlayerOnMove());
    }

    public void computerMove() {

        CheckersBoard olderBoard = checkersBoard.clone();
        olderBoard.setBoardForGetMove();

        List<Point> tah = Mozek.inteligentniVyber(Rules.getPlayerOnMove(), Mozek.HLOUBKA, checkersBoard, 1);

        Control.gameControl(tah.get(0), tah.get(1), checkersBoard);

        CheckersBoard newerBoard = checkersBoard.clone();
        newerBoard.setBoardForGetMove();

        Game.getgUserInterface().getInfoPanel().addMoveToTextArea(OperationsWithCheckersBoard.compareTwoBoardsForGetMove(olderBoard, newerBoard));
        Control.addMove = false;

        rePaint();

        if (Rules.isIndexInArray(Control.againFromPosition)) {
            computerMove();
        }
    }

    public void setPlayers(int whiteSetting, int blackSetting) {
        Control.setPlayers(whiteSetting, blackSetting);
    }

    public void enableClicksOnBoard() {
        int num = 0;

        for (int x = 0; x < boardSize; x++) {
            for (int y = 0; y < boardSize; y++) {
                if (num % 2 != 0) {
                    gCheckersBoard[x][y].setClickable(true);
                }
                if (y == 7) {
                    num++;
                }
                num++;
            }
        }
    }

    public void disableClicksOnBoard() {
        Game.getgUserInterface().getInfoPanel().setLabelInformation("");
        Game.getgUserInterface().getInfoPanel().setCoordinateLabel(new Point(-1, -1));

        int num = 0;

        for (int x = 0; x < boardSize; x++) {
            for (int y = 0; y < boardSize; y++) {
                if (num % 2 != 0) {
                    gCheckersBoard[x][y].setClickable(false);
                }
                if (y == 7) {
                    num++;
                }
                num++;
            }
        }
    }

    @Override
    public boolean isEnabled() {
        return gCheckersBoard[0][1].isClickable();
    }

    public boolean isEmpty() {
        for (int x = 0; x < boardSize; x++) {
            for (int y = 0; y < boardSize; y++) {
                if (gCheckersBoard[x][y].getBoxValue() != CheckersBoard.EMPTY_BOX) {
                    return false;
                }
            }
        }
        return true;
    }

    //<Listeners>
    private void initializeMouseAction(final CheckersBox checkersBox) {
        checkersBox.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent me) {
                if (checkersBox.isClickable()) {
                    setEnteredIcon(checkersBox);
                    setInformationLabelOnStatusBar("MOVE");
                    Game.getgUserInterface().getInfoPanel().setCoordinateLabel(checkersBox.getCoordinate());
                }
            }

            private void setInformationLabelOnStatusBar(String text) {
                if (!text.isEmpty()) {
                    if (!state.isFromPositionInitialize() && isFigureOnBoxOnMoveOrNotZero()) {
                        Game.getgUserInterface().getInfoPanel().setLabelInformation("Klikni pro tah odtud.");
                    } else if (isFigureOnBoxZero() && state.isFromPositionInitialize()) {
                        Game.getgUserInterface().getInfoPanel().setLabelInformation("Klikni pro tah sem.");
                    }
                } else {
                    Game.getgUserInterface().getInfoPanel().setLabelInformation("");
                }
            }

            private void setEnteredIcon(CheckersBox checkersBox) {
                if (!checkersBox.isMarked()) {
                    checkersBox.setIcon(checkersBox.getEnteredIcon());
                } else {
                    checkersBox.setIcon(checkersBox.getEnteredMarkedIcon());
                }
            }

            @Override
            public void mouseExited(MouseEvent me) {
                if (checkersBox.isClickable()) {
                    setExitIcon(checkersBox);
                    setInformationLabelOnStatusBar("");
                    Game.getgUserInterface().getInfoPanel().setCoordinateLabel(new Point(-1, -1));
                }
            }

            private void setExitIcon(CheckersBox checkersBox) {
                if (!checkersBox.isMarked()) {
                    checkersBox.setIcon(checkersBox.getExitIcon());
                } else {
                    checkersBox.setIcon(checkersBox.getExitMarkedIcon());
                }
            }

            @Override
            public void mouseClicked(MouseEvent me) {
                CheckersBoard olderBoard = checkersBoard.clone();
                olderBoard.setBoardForGetMove();

                if (checkersBox.isClickable()) {
                    if (!state.isFromPositionInitialize() && isFigureOnBoxOnMoveOrNotZero()) {
                        state.initializeFromPosition(checkersBox);
                        paintPossibleMoves(checkersBox.getCoordinate());
                    } else if (state.isFromPositionInitialize()) {
                        state.initializeToPosition(checkersBox);
                        moveAction();

                        if (Control.addMove) {
                            CheckersBoard newerBoard = checkersBoard.clone();
                            newerBoard.setBoardForGetMove();

                            Control.addMove = false;
                            Game.getgUserInterface().getInfoPanel().addMoveToTextArea(OperationsWithCheckersBoard.compareTwoBoardsForGetMove(olderBoard, newerBoard));
                        }

                        if (winnerChecking()) {
                            winnerAction();
                        } else if (isPlayerOnMoveComputer()) {
                            computerMove();

                            if (winnerChecking()) {
                                winnerAction();
                            }
                        }
                    }

                    mouseExited(me); //zesvetleni po kliku a prepis status baru
                    mouseEntered(me);
                }
            }

            private void moveAction() {

                Control.gameControl(new Point(state.getFromY(), state.getFromX()), new Point(state.getToY(), state.getToX()), checkersBoard);
                state.deinitializeState();
                rePaint();
            }

            public boolean isFigureOnBoxOnMoveOrNotZero() {
                Point coordinate = new Point(checkersBox.getCoordinate().y, checkersBox.getCoordinate().x);
                return checkersBoard.getPlayerFromFigure(coordinate) == Rules.getPlayerOnMove();
            }

            public boolean isFigureOnBoxZero() {
                Point coordinate = new Point(checkersBox.getCoordinate().y, checkersBox.getCoordinate().x);
                return checkersBoard.getPlayerFromFigure(coordinate) == CheckersBoard.EMPTY_BOX;
            }
        });
    }
    //</ Listeners>

    private void paintPossibleMoves(Point from) {

        List<Point> possibleMoves = Rules.generatorTahu(new Point(from.y, from.x), checkersBoard, Rules.getPlayerOnMove(), false);

        for (Point p : possibleMoves) {
            if (Rules.getPlayerOnMove() == Rules.WHITE_PLAYER) {
                gCheckersBoard[(int) p.getY()][(int) p.getX()].setIcon(boardPictures.getPossibleMoveWhite());
            } else if (Rules.getPlayerOnMove() == Rules.BLACK_PLAYER) {
                gCheckersBoard[(int) p.getY()][(int) p.getX()].setIcon(boardPictures.getPossibleMoveBlack());
            }
        }
    }

    //<Events>
    public boolean winnerChecking() {
        return (Game.getWinnerChecking().checking(checkersBoard));
    }

    public void winnerAction() {
        disableClicksOnBoard();
        Game.getgUserInterface().getInfoPanel().setLabelPlayerWinner(Game.getWinnerChecking().getWinner());
        Game.getgUserInterface().setMenusAndButtonsAfterWinning();

        WinnerDialog a = new WinnerDialog(Game.getWinnerChecking().getWinner());
        a.setVisible(true);
    }

    public void undoAction() {
        if (!isEnabled() && !arePlayersComputers()) {
            enableClicksOnBoard();
        }
        checkersBoard = Game.getHistory().getUndoMove(checkersBoard);

        rePaint();

        Game.getgUserInterface().getInfoPanel().getButtonComputersMove().setEnabled(isPlayerOnMoveComputer());
        Game.getgUserInterface().getInfoPanel().undoMoveInTextArea();

        Game.getgUserInterface().getMenuItemSetPlayers().setEnabled(true);
        Game.getgUserInterface().getMenuOptions().setEnabled(true);
        
        state.deinitializeState();

        if (!isPlayerOnMoveComputer()) {
            enableClicksOnBoard();
        } else {
            disableClicksOnBoard();
        }
    }

    public void redoAction() {
        CheckersBoard olderBoard = checkersBoard.clone();
        olderBoard.setBoardForGetMove();

        checkersBoard = Game.getHistory().getRedoMove(checkersBoard);

        CheckersBoard newerBoard = checkersBoard.clone();
        newerBoard.setBoardForGetMove();

        rePaint();

        Game.getgUserInterface().getInfoPanel().getButtonComputersMove().setEnabled(isPlayerOnMoveComputer());
        Game.getgUserInterface().getInfoPanel().redoMoveInTextArea();
        
        state.deinitializeState();

        if (!isPlayerOnMoveComputer()) {
            enableClicksOnBoard();
        } else {
            disableClicksOnBoard();
        }

        if (winnerChecking()) {
            winnerAction();
        }
    }

    public void loadAction(String path) {

        //OSETRENI VADNEHO SOUBORU

        try (FileReader r = new FileReader(path)) {
            checkersBoard = SaveAndLoad.loadGame(r);

            enableClicksOnBoard();
            rePaint();

            Game.getgUserInterface().setNeedSave(false);
            Game.getgUserInterface().getSetPlayersDialog().setPlayersSettings(Control.getWHITE_PLAYER_SETTING(), Control.getBLACK_PLAYER_SETTING());
            Game.getgUserInterface().setMenusItemInGame();

            if (!isPlayerOnMoveComputer()) {
                enableClicksOnBoard();
            } else {
                disableClicksOnBoard();
                Game.getgUserInterface().getInfoPanel().getButtonComputersMove().setEnabled(true);
            }

            state.deinitializeState();
            JOptionPane.showMessageDialog(null, "Hra byla úspěšně načtena.", "Načíst", JOptionPane.INFORMATION_MESSAGE);

            if (winnerChecking()) {
                winnerAction();
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Hru se bohužel nepodařilo načíst.", "Chyba", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void saveAction(String path) {

        try (FileWriter w = new FileWriter(path + ".sav")) {
            SaveAndLoad.saveGame(w, checkersBoard);
            JOptionPane.showMessageDialog(null, "Hra byla úspěšně uložena.", "Uložit", JOptionPane.INFORMATION_MESSAGE);
            Game.getgUserInterface().setNeedSave(false);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Hru se bohužel nepodařilo uložit.", "Chyba", JOptionPane.ERROR_MESSAGE);
        }
    }
    //</ Events>
    //

    private void setLettersAndNumbers() {
        GridBagConstraints position = new GridBagConstraints();

        for (int y = 0; y < boardSize; y++) {

            CheckersBox checkersLetters = new CheckersBox(boardPictures.getLettersBox().get(y));
            CheckersBox checkersNumbers = new CheckersBox(boardPictures.getNumbersBox().get(7 - y));

            position.gridx = y + 1;
            position.gridy = 0;
            this.gamePanel.add(checkersLetters.clone(), position); //horni pismena

            position.gridx = 0;
            position.gridy = y + 1;
            this.gamePanel.add(checkersNumbers.clone(), position); //leva cisla

            position.gridx = y + 1;
            position.gridy = boardSize + 1;
            this.gamePanel.add(checkersLetters.clone(), position); //dolni pismena

            position.gridx = boardSize + 1;
            position.gridy = y + 1;
            this.gamePanel.add(checkersNumbers.clone(), position); //prava cisla
        }
    }

    private void setCorners() {
        GridBagConstraints position = new GridBagConstraints();

        CheckersBox checkersBox = new CheckersBox(boardPictures.getCornerBox());

        position.gridx = 0;
        position.gridy = 0;

        this.gamePanel.add(checkersBox.clone(), position);
        position.gridx = 0;
        position.gridy = 9;

        this.gamePanel.add(checkersBox.clone(), position);
        position.gridx = 9;
        position.gridy = 0;

        this.gamePanel.add(checkersBox.clone(), position);
        position.gridx = 9;
        position.gridy = 9;

        this.gamePanel.add(checkersBox.clone(), position);
    }

    private void setCheckersBoxIcon(CheckersBox checkersBox) {
        if (!checkersBox.isMarked()) {
            checkersBox.setIcon(checkersBox.getExitIcon());
        } else {
            checkersBox.setIcon(checkersBox.getExitMarkedIcon());
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(boardPictures.getBackgroundImageForGamePanel().getImage(), 0, 0, null);
    }
}
