/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import mediator.Game;
import parachutecheckers.Rules;

/**
 *
 * @author KubiX
 */
public class WinnerDialog extends JDialog {

    private WinnerPanel winnerPanel;
    private BoardPictures boardPictures = Game.getBoardPictures();
    private Point initialClick;

    public WinnerDialog(int player) {

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            System.out.println(e.getMessage());
        }

        if (player == Rules.WHITE_PLAYER) {
            this.winnerPanel = new WinnerPanel(boardPictures.getWhiteWinner());
        } else {
            this.winnerPanel = new WinnerPanel(boardPictures.getBlackWinner());
        }


        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        this.add(winnerPanel, BorderLayout.CENTER);

        this.setPreferredSize(new Dimension(420, 300));

        this.setModal(true);

        this.setResizable(false);

        this.setUndecorated(true);

        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                setVisible(false);
                dispose();
            }

            @Override
            public void mousePressed(MouseEvent e) {
                initialClick = e.getPoint();
                getComponentAt(initialClick);
            }
        });

        addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {

                // get location of Window
                int thisX = getLocation().x;
                int thisY = getLocation().y;

                // Determine how much the mouse moved since the initial click
                int xMoved = (thisX + e.getX()) - (thisX + initialClick.x);
                int yMoved = (thisY + e.getY()) - (thisY + initialClick.y);

                // Move window to this position
                int X = thisX + xMoved;
                int Y = thisY + yMoved;

                setLocation(X, Y);
                Game.getgUserInterface().setLocation(X - 256, Y - 196);
            }
        });

        pack();
    }

    @Override
    public void setVisible(boolean visible) {
        this.setLocationRelativeTo(Game.getgUserInterface());

        super.setVisible(visible);
    }
}
