package gui;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import mediator.Game;

public class BoardPictures {

    private File nullBox;
    private File emptyBox;
    //
    private File whitePawn;
    private File blackPawn;
    private File whiteParachutist;
    private File whiteCheckers;
    private File blackParachutist;
    private File blackCheckers;
    //
    private File mouseEnteredEmptyBox;
    private File mouseEnteredWhitePawn;
    private File mouseEnteredBlackPawn;
    private File mouseEnteredWhiteParachutist;
    private File mouseEnteredWhiteCheckers;
    private File mouseEnteredBlackParachutist;
    private File mouseEnteredBlackCheckers;
    //
    private File markedWhitePawn;
    private File markedBlackPawn;
    private File markedWhiteParachutist;
    private File markedWhiteCheckers;
    private File markedBlackParachutist;
    private File markedBlackCheckers;
    //
    private File markedMouseEnteredWhitePawn;
    private File markedMouseEnteredBlackPawn;
    private File markedMouseEnteredWhiteParachutist;
    private File markedMouseEnteredWhiteCheckers;
    private File markedMouseEnteredBlackParachutist;
    private File markedMouseEnteredBlackCheckers;
    //
    private File possibleMoveWhite;
    private File possibleMoveBlack;
    //
    private File numberBox1;
    private File numberBox2;
    private File numberBox3;
    private File numberBox4;
    private File numberBox5;
    private File numberBox6;
    private File numberBox7;
    private File numberBox8;
    //
    private File letterBoxA;
    private File letterBoxB;
    private File letterBoxC;
    private File letterBoxD;
    private File letterBoxE;
    private File letterBoxF;
    private File letterBoxG;
    private File letterBoxH;
    //
    private File cornerBox;
    //
    private File blackIsOnMove;
    private File whiteIsOnMove;
    private File whiteIsWinner;
    private File blackIsWinner;
    private File logo;
    private File welcome;
    //
    private File whiteWinner;
    private File blackWinner;
    //
    private File backgroundImageForGamePanel;
    private File backgroundImageForInfoPanel;
    private File backgroundImageForPlayersChoose;
    //
    private File exclamationMark;
    private File icon;
    //
    private File newGameIcon;
    private File loadIcon;
    private File saveIcon;
    private File exitIcon;
    private File undoIcon;
    private File redoIcon;
    private File settingsIcon;
    //
    private String path;
    private final String terminal = ".png";
    private int boxSize = 70;

    public BoardPictures() {

        path = "imgs/";
        //
        this.nullBox = new File(path + "nullBox" + terminal);
        this.emptyBox = new File(path + "emptyBox" + terminal);
        //
        this.whitePawn = new File(path + "whitePawn" + terminal);
        this.blackPawn = new File(path + "blackPawn" + terminal);
        //
        this.mouseEnteredEmptyBox = new File(path + "mouseEnteredEmptyBox" + terminal);
        this.mouseEnteredWhitePawn = new File(path + "mouseEnteredWhitePawn" + terminal);
        this.mouseEnteredBlackPawn = new File(path + "mouseEnteredBlackPawn" + terminal);
        //
        this.markedWhitePawn = new File(path + "markedWhitePawn" + terminal);
        this.markedBlackPawn = new File(path + "markedBlackPawn" + terminal);
        //
        this.markedMouseEnteredWhitePawn = new File(path + "mouseEnteredMarkedWhitePawn" + terminal);
        this.markedMouseEnteredBlackPawn = new File(path + "mouseEnteredMarkedBlackPawn" + terminal);
        //
        this.whiteParachutist = new File(path + "whiteParachutist" + terminal);
        this.whiteCheckers = new File(path + "whiteCheckers" + terminal);
        this.blackParachutist = new File(path + "blackParachutist" + terminal);
        this.blackCheckers = new File(path + "blackCheckers" + terminal);
        //
        this.mouseEnteredWhiteParachutist = new File(path + "mouseEnteredWhiteParachutist" + terminal);
        this.mouseEnteredWhiteCheckers = new File(path + "mouseEnteredWhiteCheckers" + terminal);
        this.mouseEnteredBlackParachutist = new File(path + "mouseEnteredBlackParachutist" + terminal);
        this.mouseEnteredBlackCheckers = new File(path + "mouseEnteredBlackCheckers" + terminal);
        //
        this.markedWhiteParachutist = new File(path + "markedWhiteParachutist" + terminal);
        this.markedWhiteCheckers = new File(path + "markedWhiteCheckers" + terminal);
        this.markedBlackParachutist = new File(path + "markedBlackParachutist" + terminal);
        this.markedBlackCheckers = new File(path + "markedBlackCheckers" + terminal);
        //
        this.markedMouseEnteredWhiteParachutist = new File(path + "mouseEnteredMarkedWhiteParachutist" + terminal);
        this.markedMouseEnteredWhiteCheckers = new File(path + "mouseEnteredMarkedWhiteCheckers" + terminal);
        this.markedMouseEnteredBlackParachutist = new File(path + "mouseEnteredMarkedBlackParachutist" + terminal);
        this.markedMouseEnteredBlackCheckers = new File(path + "mouseEnteredMarkedBlackCheckers" + terminal);
        //
        this.possibleMoveWhite = new File(path + "possibleMoveWhite" + terminal);
        this.possibleMoveBlack = new File(path + "possibleMoveBlack" + terminal);
        //
        this.numberBox1 = new File(path + "numbers/number1" + terminal);
        this.numberBox2 = new File(path + "numbers/number2" + terminal);
        this.numberBox3 = new File(path + "numbers/number3" + terminal);
        this.numberBox4 = new File(path + "numbers/number4" + terminal);
        this.numberBox5 = new File(path + "numbers/number5" + terminal);
        this.numberBox6 = new File(path + "numbers/number6" + terminal);
        this.numberBox7 = new File(path + "numbers/number7" + terminal);
        this.numberBox8 = new File(path + "numbers/number8" + terminal);
        //
        this.letterBoxA = new File(path + "letters/letterA" + terminal);
        this.letterBoxB = new File(path + "letters/letterB" + terminal);
        this.letterBoxC = new File(path + "letters/letterC" + terminal);
        this.letterBoxD = new File(path + "letters/letterD" + terminal);
        this.letterBoxE = new File(path + "letters/letterE" + terminal);
        this.letterBoxF = new File(path + "letters/letterF" + terminal);
        this.letterBoxG = new File(path + "letters/letterG" + terminal);
        this.letterBoxH = new File(path + "letters/letterH" + terminal);
        //
        this.whiteIsOnMove = new File(path + "whiteIsOnMove" + terminal);
        this.whiteIsWinner = new File(path + "whiteIsWinner" + terminal);
        this.blackIsOnMove = new File(path + "blackIsOnMove" + terminal);
        this.blackIsWinner = new File(path + "blackIsWinner" + terminal);
        this.welcome = new File(path + "welcome" + terminal);
        this.logo = new File(path + "logo" + terminal);
        //
        this.whiteWinner = new File(path + "whiteWinner" + terminal);
        this.blackWinner = new File(path + "blackWinner" + terminal);
        //
        //
        this.backgroundImageForGamePanel = new File(path + "backgroundForGamePanel" + terminal);
        this.backgroundImageForInfoPanel = new File(path + "backgroundForInfoPanel" + terminal);
        this.backgroundImageForPlayersChoose = new File(path + "playersChooseBackground" + terminal);
        //
        this.cornerBox = new File(path + "corner" + terminal);
        //
        this.exclamationMark = new File(path + "exclamation" + terminal);
        this.icon = new File(path + "icon" + terminal);
        //
        this.newGameIcon = new File(path + "icons/newGameIcon" + terminal);
        this.loadIcon = new File(path + "icons/loadIcon" + terminal);
        this.saveIcon = new File(path + "icons/saveIcon" + terminal);
        this.exitIcon = new File(path + "icons/exitIcon" + terminal);
        this.undoIcon = new File(path + "icons/undoIcon" + terminal);
        this.redoIcon = new File(path + "icons/redoIcon" + terminal);
        this.settingsIcon = new File(path + "icons/settingsIcon" + terminal);

        checkFilesExists();
    }

    private void checkFilesExists() {
        for (File file : getFiles()) {
            if (!file.exists()) {
                JOptionPane.showMessageDialog(null, "Soubor \""+file.getName()+"\" nebyl nalezen !", "Ok", JOptionPane.ERROR_MESSAGE);
                System.exit(1);
            }
        }
    }

    private ArrayList<File> getFiles() {
        ArrayList<File> files = new ArrayList<>();

        files.add(nullBox);
        files.add(emptyBox);
        //
        files.add(whitePawn);
        files.add(blackPawn);
        files.add(whiteParachutist);
        files.add(whiteCheckers);
        files.add(blackParachutist);
        files.add(blackCheckers);
        //
        files.add(mouseEnteredEmptyBox);
        files.add(mouseEnteredWhitePawn);
        files.add(mouseEnteredBlackPawn);
        files.add(mouseEnteredWhiteParachutist);
        files.add(mouseEnteredWhiteCheckers);
        files.add(mouseEnteredBlackParachutist);
        files.add(mouseEnteredBlackCheckers);
        //
        files.add(markedWhitePawn);
        files.add(markedBlackPawn);
        files.add(markedWhiteParachutist);
        files.add(markedWhiteCheckers);
        files.add(markedBlackParachutist);
        files.add(markedBlackCheckers);
        //
        files.add(markedMouseEnteredWhitePawn);
        files.add(markedMouseEnteredBlackPawn);
        files.add(markedMouseEnteredWhiteParachutist);
        files.add(markedMouseEnteredWhiteCheckers);
        files.add(markedMouseEnteredBlackParachutist);
        files.add(markedMouseEnteredBlackCheckers);
        //
        files.add(possibleMoveWhite);
        files.add(possibleMoveBlack);
        //
        files.add(numberBox1);
        files.add(numberBox2);
        files.add(numberBox3);
        files.add(numberBox4);
        files.add(numberBox5);
        files.add(numberBox6);
        files.add(numberBox7);
        files.add(numberBox8);
        //
        files.add(letterBoxA);
        files.add(letterBoxB);
        files.add(letterBoxC);
        files.add(letterBoxD);
        files.add(letterBoxE);
        files.add(letterBoxF);
        files.add(letterBoxG);
        files.add(letterBoxH);
        //
        files.add(cornerBox);
        //
        files.add(blackIsOnMove);
        files.add(whiteIsOnMove);
        files.add(whiteIsWinner);
        files.add(blackIsWinner);
        files.add(logo);
        files.add(welcome);
        //
        files.add(whiteWinner);
        files.add(blackWinner);
        //
        files.add(backgroundImageForGamePanel);
        files.add(backgroundImageForInfoPanel);
        files.add(backgroundImageForPlayersChoose);
        //
        files.add(exclamationMark);
        files.add(icon);
        //
        files.add(newGameIcon);
        files.add(loadIcon);
        files.add(saveIcon);
        files.add(exitIcon);
        files.add(undoIcon);
        files.add(redoIcon);
        files.add(settingsIcon);

        return files;
    }

    private static ImageIcon getImageIconFromFile(File file) {
        return new ImageIcon(file.getAbsolutePath());
    }

    public ImageIcon getPossibleMoveWhite() {
        return getImageIconFromFile(possibleMoveWhite);
    }

    public ImageIcon getBackgroundForPlayersChoose() {
        return getImageIconFromFile(backgroundImageForPlayersChoose);
    }

    public ImageIcon getPossibleMoveBlack() {
        return getImageIconFromFile(possibleMoveBlack);
    }

    public ImageIcon getBlackIsOnMove() {
        return getImageIconFromFile(blackIsOnMove);
    }

    public ImageIcon getWhiteIsOnMove() {
        return getImageIconFromFile(whiteIsOnMove);
    }

    public ImageIcon getWhiteIsWinner() {
        return getImageIconFromFile(whiteIsWinner);
    }

    public ImageIcon getBlackIsWinner() {
        return getImageIconFromFile(blackIsWinner);
    }

    public ImageIcon getLogo() {
        return getImageIconFromFile(logo);
    }

    public ImageIcon getWelcome() {
        return getImageIconFromFile(welcome);
    }

    public ImageIcon getUndoIcon() {
        return getImageIconFromFile(undoIcon);
    }

    public ImageIcon getBackgroundImageForGamePanel() {
        return getImageIconFromFile(backgroundImageForGamePanel);
    }

    public ImageIcon getBackgroundImageForInfoPanel() {
        return getImageIconFromFile(backgroundImageForInfoPanel);
    }

    public ImageIcon getCornerBox() {
        return getImageIconFromFile(cornerBox);
    }

    public ImageIcon getNewGameIcon() {
        return getImageIconFromFile(newGameIcon);
    }

    public ImageIcon getLoadIcon() {
        return getImageIconFromFile(loadIcon);
    }

    public ImageIcon getSaveIcon() {
        return getImageIconFromFile(saveIcon);
    }

    public ImageIcon getExitIcon() {
        return getImageIconFromFile(exitIcon);
    }

    public ImageIcon getRedoIcon() {
        return getImageIconFromFile(redoIcon);
    }

    public ImageIcon getSettingsIcon() {
        return getImageIconFromFile(settingsIcon);
    }

    public ArrayList<ImageIcon> getLettersBox() {
        ArrayList<ImageIcon> checkersLettersList = new ArrayList<>();

        checkersLettersList.add(getImageIconFromFile(letterBoxA));
        checkersLettersList.add(getImageIconFromFile(letterBoxB));
        checkersLettersList.add(getImageIconFromFile(letterBoxC));
        checkersLettersList.add(getImageIconFromFile(letterBoxD));
        checkersLettersList.add(getImageIconFromFile(letterBoxE));
        checkersLettersList.add(getImageIconFromFile(letterBoxF));
        checkersLettersList.add(getImageIconFromFile(letterBoxG));
        checkersLettersList.add(getImageIconFromFile(letterBoxH));

        return checkersLettersList;
    }

    public ArrayList<ImageIcon> getNumbersBox() {
        ArrayList<ImageIcon> checkersNumbersList = new ArrayList<>();

        checkersNumbersList.add(getImageIconFromFile(numberBox1));
        checkersNumbersList.add(getImageIconFromFile(numberBox2));
        checkersNumbersList.add(getImageIconFromFile(numberBox3));
        checkersNumbersList.add(getImageIconFromFile(numberBox4));
        checkersNumbersList.add(getImageIconFromFile(numberBox5));
        checkersNumbersList.add(getImageIconFromFile(numberBox6));
        checkersNumbersList.add(getImageIconFromFile(numberBox7));
        checkersNumbersList.add(getImageIconFromFile(numberBox8));

        return checkersNumbersList;
    }

    public ImageIcon getNullBox() {
        return getImageIconFromFile(nullBox);
    }

    public ImageIcon getEmptyBox() {
        return getImageIconFromFile(emptyBox);
    }

    public ImageIcon getWhiteWinner() {
        return getImageIconFromFile(whiteWinner);
    }

    public ImageIcon getBlackWinner() {
        return getImageIconFromFile(blackWinner);
    }

    public ImageIcon getWhitePawn() {
        return getImageIconFromFile(whitePawn);
    }

    public ImageIcon getBlackPawn() {
        return getImageIconFromFile(blackPawn);
    }

    public ImageIcon getWhiteParachutist() {
        return getImageIconFromFile(whiteParachutist);
    }

    public ImageIcon getWhiteCheckers() {
        return getImageIconFromFile(whiteCheckers);
    }

    public ImageIcon getExclamationMark() {
        return getImageIconFromFile(exclamationMark);
    }

    public ImageIcon getBlackParachutist() {
        return getImageIconFromFile(blackParachutist);
    }

    public ImageIcon getBlackCheckers() {
        return getImageIconFromFile(blackCheckers);
    }

    public ImageIcon getMouseEnteredEmptyBox() {
        return getImageIconFromFile(mouseEnteredEmptyBox);
    }

    public ImageIcon getMouseEnteredWhitePawn() {
        return getImageIconFromFile(mouseEnteredWhitePawn);
    }

    public ImageIcon getMouseEnteredBlackPawn() {
        return getImageIconFromFile(mouseEnteredBlackPawn);
    }

    public ImageIcon getMouseEnteredWhiteParachutist() {
        return getImageIconFromFile(mouseEnteredWhiteParachutist);
    }

    public ImageIcon getMouseEnteredWhiteCheckers() {
        return getImageIconFromFile(mouseEnteredWhiteCheckers);
    }

    public ImageIcon getMouseEnteredBlackParachutist() {
        return getImageIconFromFile(mouseEnteredBlackParachutist);
    }

    public ImageIcon getMouseEnteredBlackCheckers() {
        return getImageIconFromFile(mouseEnteredBlackCheckers);
    }

    public ImageIcon getMarkedWhitePawn() {
        return getImageIconFromFile(markedWhitePawn);
    }

    public ImageIcon getMarkedBlackPawn() {
        return getImageIconFromFile(markedBlackPawn);
    }

    public ImageIcon getMarkedWhiteParachutist() {
        return getImageIconFromFile(markedWhiteParachutist);
    }

    public ImageIcon getMarkedWhiteCheckers() {
        return getImageIconFromFile(markedWhiteCheckers);
    }

    public ImageIcon getMarkedBlackParachutist() {
        return getImageIconFromFile(markedBlackParachutist);
    }

    public ImageIcon getMarkedBlackCheckers() {
        return getImageIconFromFile(markedBlackCheckers);
    }

    public ImageIcon getMarkedMouseEnteredWhitePawn() {
        return getImageIconFromFile(markedMouseEnteredWhitePawn);
    }

    public ImageIcon getMarkedMouseEnteredBlackPawn() {
        return getImageIconFromFile(markedMouseEnteredBlackPawn);
    }

    public ImageIcon getMarkedMouseEnteredWhiteParachutist() {
        return getImageIconFromFile(markedMouseEnteredWhiteParachutist);
    }

    public ImageIcon getMarkedMouseEnteredWhiteCheckers() {
        return getImageIconFromFile(markedMouseEnteredWhiteCheckers);
    }

    public ImageIcon getMarkedMouseEnteredBlackParachutist() {
        return getImageIconFromFile(markedMouseEnteredBlackParachutist);
    }

    public ImageIcon getIcon() {
        return getImageIconFromFile(icon);
    }

    public ImageIcon getMarkedMouseEnteredBlackCheckers() {
        return getImageIconFromFile(markedMouseEnteredBlackCheckers);
    }

    public int getBoxSize() {
        return boxSize;
    }
}
