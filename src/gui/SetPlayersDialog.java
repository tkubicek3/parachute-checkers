/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import mediator.Game;
import parachutecheckers.Control;

/**
 *
 * @author KubiX
 */
public class SetPlayersDialog extends JDialog {

    private SetPlayersPanel setPlayersPanel;
    private Point position;
    private boolean canceled;

    public SetPlayersDialog() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        this.setPlayersPanel = new SetPlayersPanel();
        this.add(setPlayersPanel, BorderLayout.CENTER);

        this.setPreferredSize(new Dimension(406, 366));
        this.setResizable(false);

        this.addWindowListener(new ExitFromWindow());

        this.setModal(true);

        this.setPlayersPanel.getButtonOk().addActionListener(new buttonOkEvent());
        this.setPlayersPanel.getButtonCancel().addActionListener(new buttonCancelEvent());
        //***Keys***
        InputMap map = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        map.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "ok");
        map.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "cancel");

        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put("ok", new actionOk());
        actionMap.put("cancel", new actionCancel());
        //***    ****
        
        this.pack();
    }

    public static void addEscapeListener(final JDialog dialog) {
        ActionListener escListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog.setVisible(false);
            }
        };
    }

    public void setVisibled(boolean visible) {
        if (visible) {
            saveActualStateOfDialog();
        }

        this.setLocationRelativeTo(Game.getgUserInterface());
        this.setVisible(visible);
    }
    //
    private ArrayList<Object> actualState;

    public void saveActualStateOfDialog() {
        actualState = new ArrayList<>();

        actualState.add(setPlayersPanel.getRadioButtonWhiteHuman().isSelected());
        actualState.add(setPlayersPanel.getRadioButtonWhiteComputer().isSelected());
        //
        actualState.add(setPlayersPanel.getRadioButtonBlackHuman().isSelected());
        actualState.add(setPlayersPanel.getRadioButtonBlackComputer().isSelected());
        //
        actualState.add(setPlayersPanel.getComboBoxWhite().getSelectedIndex());
        actualState.add(setPlayersPanel.getComboBoxBlack().getSelectedIndex());
    }

    public void loadSavedStateOfDialog() {
        setPlayersPanel.getRadioButtonWhiteHuman().setSelected((boolean) actualState.get(0));
        setPlayersPanel.getRadioButtonWhiteComputer().setSelected((boolean) actualState.get(1));
        //
        setPlayersPanel.getRadioButtonBlackHuman().setSelected((boolean) actualState.get(2));
        setPlayersPanel.getRadioButtonBlackComputer().setSelected((boolean) actualState.get(3));
        //
        setPlayersPanel.getComboBoxWhite().setSelectedIndex((int) actualState.get(4));
        setPlayersPanel.getComboBoxBlack().setSelectedIndex((int) actualState.get(5));
        //
        setPlayersPanel.getComboBoxWhite().setEnabled(setPlayersPanel.getRadioButtonWhiteComputer().isSelected());
        setPlayersPanel.getComboBoxBlack().setEnabled(setPlayersPanel.getRadioButtonBlackComputer().isSelected());
    }

    public void setPlayersSettings(int whitePlayer, int blackPlayer) {
        if (whitePlayer == Control.HUMAN_PLAYER) {
            setPlayersPanel.getRadioButtonWhiteHuman().setSelected(true);
        } else {
            setPlayersPanel.getRadioButtonWhiteComputer().setSelected(true);
        }

        if (blackPlayer == Control.HUMAN_PLAYER) {
            setPlayersPanel.getRadioButtonBlackHuman().setSelected(true);
        } else {
            setPlayersPanel.getRadioButtonBlackComputer().setSelected(true);
        }
    }

    //Events
    private class ExitFromWindow extends WindowAdapter {

        @Override
        public void windowClosing(WindowEvent we) {
            new actionCancel().actionPerformed(null);
        }
    }

    private class buttonOkEvent implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            new actionOk().actionPerformed(ae);
        }
    }

    private class actionOk extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent ae) {
            canceled = false;
            setVisible(false);
            dispose();
        }
    }

    private class buttonCancelEvent implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            new actionCancel().actionPerformed(ae);
        }
    }

    private class actionCancel extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent ae) {
            canceled = true;
            loadSavedStateOfDialog();
            setVisible(false);
            dispose();
        }
    }

    //Getters
    public int getWhiteSetting() {
        return setPlayersPanel.getWhiteSetting();
    }

    public int getBlackSetting() {
        return setPlayersPanel.getBlackSetting();
    }

    public int getWhiteComputerDifficult() {
        return setPlayersPanel.getWhiteComputerDifficult();
    }

    public int getBlackComputerDifficult() {
        return setPlayersPanel.getBlackComputerDifficult();
    }

    public boolean isCanceled() {
        return canceled;
    }
}
